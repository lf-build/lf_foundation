﻿namespace LendFoundry.Foundation.Client
{
    public class FileDetail
    {
        public string Name { get; set; }
        public string FileName { get; set; }
        public byte[] FileBytes { get; set; }
    }
}
