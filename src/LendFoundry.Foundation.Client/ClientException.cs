using System;

namespace LendFoundry.Foundation.Client
{
    public class ClientException : Exception
    {
        public ClientException(string requestInfo, Error error) : base($"[{requestInfo}] Error {error.Code}: {error.Message}")
        {
            Error = error;
        }

        public Error Error { get; set; }
    }
}