using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Net.Http;
using System.IO;
using System.Collections.Generic;
#if DOTNET2
using Microsoft.AspNetCore.Http;
#else
using Microsoft.AspNet.Http;
#endif

namespace LendFoundry.Foundation.Client
{
    public class ServiceClient : IServiceClient
    {
        private const int MaxRetries = 0;
        private const string Authorization = "Authorization";
        private const string RequestId = "X-Request-Id";

        public ServiceClient(IHttpContextAccessor accessor, ILogger logger, ITokenReader tokenReader, Uri uri)
        {
            if (accessor == null)
                throw new ArgumentNullException(nameof(accessor));

            if (logger == null)
                throw new ArgumentNullException(nameof(logger));

            if (tokenReader == null)
                throw new ArgumentNullException(nameof(tokenReader));

            if (uri == null)
                throw new ArgumentNullException(nameof(uri));

            Accessor = accessor;
            Logger = logger;
            TokenReader = tokenReader;

            Client = new RestClient(uri);
        }

        [Obsolete("This is obsolate need to replace it with overloaded method with Uri")]
        public ServiceClient(IHttpContextAccessor accessor, ILogger logger, ITokenReader tokenReader, string endpoint, int port = 5000)
        {
            if (accessor == null)
                throw new ArgumentNullException(nameof(accessor));

            if (logger == null)
                throw new ArgumentNullException(nameof(logger));

            if (tokenReader == null)
                throw new ArgumentNullException(nameof(tokenReader));

            if (string.IsNullOrEmpty(endpoint))
                throw new ArgumentNullException(nameof(endpoint));

            Accessor = accessor;
            Logger = logger;
            TokenReader = tokenReader;

            Client = new RestClient(new UriBuilder("http", endpoint, port).Uri);
        }

        [Obsolete("This is obsolate need to replace it with overloaded method with Uri")]
        public ServiceClient(IHttpContextAccessor accessor, ILogger logger, ITokenReader tokenReader, IRestClient client)
        {
            if (accessor == null)
                throw new ArgumentNullException(nameof(accessor));

            if (logger == null)
                throw new ArgumentNullException(nameof(logger));

            if (tokenReader == null)
                throw new ArgumentNullException(nameof(tokenReader));

            if (client == null)
                throw new ArgumentNullException(nameof(client));

            Accessor = accessor;
            Logger = logger;
            TokenReader = tokenReader;
            Client = client;
        }

        private IRestClient Client { get; }

        private IHttpContextAccessor Accessor { get; }

        private ILogger Logger { get; }

        private ITokenReader TokenReader { get; }

        public T Execute<T>(IRestRequest request)
        {
            return ExecuteWithRetry(() =>
            {
                var response = Client.Execute(request);
                if (response == null) return default(T);

                if (response.ResponseStatus == ResponseStatus.Completed)
                {
                    switch (response.StatusCode)
                    {
                        case HttpStatusCode.OK:
                            return JsonConvert.DeserializeObject<T>(response.Content);
                        case HttpStatusCode.NoContent:
                        case HttpStatusCode.Created:
                        case HttpStatusCode.NotFound:
                        case HttpStatusCode.Found:
                            return default(T);
                        default:

                            var error = !string.IsNullOrEmpty(response.Content)
                                ? JsonConvert.DeserializeObject<Error>(response.Content)
                                : null;

                            if (error != null)
                                throw new ClientException(GetRequestInfo(request), error);

                            return default(T);
                    }
                }

                if (response.ErrorException != null)
                    throw response.ErrorException;

                return default(T);

            }, request);
        }

        public bool Execute(IRestRequest request)
        {
            return ExecuteWithRetry(() =>
            {
                var response = Client.Execute(request);
                if (response == null) return false;

                if (response.ResponseStatus == ResponseStatus.Completed)
                {
                    switch (response.StatusCode)
                    {
                        case HttpStatusCode.OK:
                        case HttpStatusCode.NoContent:
                        case HttpStatusCode.Created:
                        case HttpStatusCode.Found:
                            return true;
                        case HttpStatusCode.NotFound:
                            return false;
                        default:
                            var error = !string.IsNullOrEmpty(response.Content)
                                ? JsonConvert.DeserializeObject<Error>(response.Content)
                                : null;
                            if (error != null)
                                throw new ClientException(GetRequestInfo(request), error);
                            return false;
                    }
                }
                if (response.ErrorException != null)
                    throw response.ErrorException;
                return false;
            }, request);
        }

        public object Execute(IRestRequest request, Type type)
        {
            return ExecuteWithRetry(() =>
            {
                var response = Client.Execute(request);
                if (response == null) return default(object);

                if (response.ResponseStatus == ResponseStatus.Completed)
                {
                    switch (response.StatusCode)
                    {
                        case HttpStatusCode.OK:
                            return JsonConvert.DeserializeObject(response.Content);
                        case HttpStatusCode.NoContent:
                        case HttpStatusCode.Created:
                        case HttpStatusCode.NotFound:
                        case HttpStatusCode.Found:
                            return default(object);
                        default:

                            var error = !string.IsNullOrEmpty(response.Content)
                                ? JsonConvert.DeserializeObject<Error>(response.Content)
                                : null;

                            if (error != null)
                                throw new ClientException(GetRequestInfo(request), error);

                            return default(object);
                    }
                }

                if (response.ErrorException != null)
                    throw response.ErrorException;

                return default(object);

            }, request);
        }

        public IRestResponse ExecuteRequest(IRestRequest request)
        {
            return ExecuteWithRetry(() =>
            {
                var response = Client.Execute(request);
                if (response == null) return null;

                if (response.ResponseStatus == ResponseStatus.Completed)
                {
                    switch (response.StatusCode)
                    {
                        case HttpStatusCode.OK:
                        case HttpStatusCode.NoContent:
                        case HttpStatusCode.Created:
                        case HttpStatusCode.Found:
                            return response;
                        case HttpStatusCode.NotFound:
                            return null;
                        default:

                            var error = !string.IsNullOrEmpty(response.Content)
                                ? JsonConvert.DeserializeObject<Error>(response.Content)
                                : null;

                            if (error != null)
                                throw new ClientException(GetRequestInfo(request), error);

                            return response;
                    }
                }
                if (response.ErrorException != null)
                    throw response.ErrorException;
                return null;
            }, request);
        }

        public async Task<T> ExecuteAsync<T>(IRestRequest request)
        {
            return await Task.Run(() => Execute<T>(request));
        }

        public async Task<bool> ExecuteAsync(IRestRequest request)
        {
            return await Task.Run(() => Execute(request));
        }

        public async Task<object> ExecuteAsync(IRestRequest request, Type type)
        {
            return await Task.Run(() => Execute(request, type));
        }

        public async Task<IRestResponse> ExecuteRequestAsync(IRestRequest request)
        {
            return await Task.Run(() => ExecuteRequest(request));
        }

        private T ExecuteWithRetry<T>(Func<T> expression, IRestRequest request)
        {
            request.DateFormat = "yyyy-MM-dd";
            request.AddHeader(RequestId, GetRequestId());
            request.AddHeader(Authorization, GetToken());

            try
            {
                return expression();
            }
            catch (Exception exception)
            {
                LogErrorOnExecute(request, exception);
                throw;
            }
        }

        private void LogErrorOnExecute(IRestRequest request, Exception exception)
        {
            Logger.Error("Failed request to {endpoint}/{request}", exception, new
            {
                endpoint = Client.BaseUrl,
                request = request?.Resource ?? string.Empty
            });
        }

        private string GetToken()
        {
            return $"Bearer {TokenReader.Read()}";
        }

        private string GetRequestId()
        {
            if (Accessor.HttpContext != null)
            {
                var headers = Accessor.HttpContext.Request.Headers;
                if (headers.ContainsKey(RequestId) && !string.IsNullOrEmpty(headers[RequestId]))
                    return headers[RequestId].FirstOrDefault();
            }
            return Guid.NewGuid().ToString("N");
        }

        private string GetRequestInfo(IRestRequest request)
        {
            return $"{request.Method} {Client.BaseUrl}{request.Resource}";
        }

        private static bool ShouldRetry(Exception exception)
        {
            var clientException = exception as ClientException;
            if (clientException?.Error != null && clientException.Error.Code == 500)
                return true;

            var webException = exception as WebException;
            if (webException != null)
            {
                switch (webException.Status)
                {
                    case WebExceptionStatus.NameResolutionFailure:
                    case WebExceptionStatus.ConnectFailure:
                    case WebExceptionStatus.ReceiveFailure:
                    case WebExceptionStatus.SendFailure:
                    case WebExceptionStatus.PipelineFailure:
                    case WebExceptionStatus.RequestCanceled:
                    case WebExceptionStatus.ProtocolError:
                    case WebExceptionStatus.ConnectionClosed:
                    case WebExceptionStatus.ServerProtocolViolation:
                    case WebExceptionStatus.KeepAliveFailure:
                    case WebExceptionStatus.Timeout:
                    case WebExceptionStatus.ProxyNameResolutionFailure:
                    case WebExceptionStatus.UnknownError:
                        return true;
                }
            }

            return false;
        }

        public async Task<T> GetAsync<T>(string uri, Dictionary<string, string> queryParameters = null) where  T : class, new()
        {
            var request = new RestRequest(uri.TrimStart('/'), Method.GET);
            SetQueryParameters(request, queryParameters);
            return await ExecuteAsync<T>(request);
        }

        public async Task DeleteAsync(string uri, Dictionary<string, string> queryParameters = null)
        {
            var request = new RestRequest(uri.TrimStart('/'), Method.DELETE);
            SetQueryParameters(request, queryParameters);
            await ExecuteAsync(request);
        }

        public async Task DeleteAsync<TRequest>(string uri, TRequest data, bool bodyAsRestParameter = false, Dictionary<string, string> queryParameters = null)
        {
            var request = new RestRequest(uri.TrimStart('/'), Method.DELETE);
            AddJson(request, bodyAsRestParameter, data);
            SetQueryParameters(request, queryParameters);
            await ExecuteAsync(request);
        }

        public async Task<byte[]> GetByteArrayAsync(string uri, Dictionary<string, string> queryParameters = null)
        {
            var request = new RestRequest(uri.TrimStart('/'), Method.GET);
            SetQueryParameters(request, queryParameters);
            var result = await ExecuteRequestAsync(request);
            return result.RawBytes;
        }

        public async Task<Stream> GetStreamAsync(string uri, Dictionary<string, string> queryParameters = null)
        {
            var request = new RestRequest(uri.TrimStart('/'), Method.GET);
            SetQueryParameters(request, queryParameters);
            var result = await ExecuteRequestAsync(request);
            return new MemoryStream(result.RawBytes);
        }

        public async Task<string> GetStringAsync(string uri, Dictionary<string, string> queryParameters = null)
        {
            var request = new RestRequest(uri.TrimStart('/'), Method.GET);
            SetQueryParameters(request, queryParameters);
            var result = await ExecuteRequestAsync(request);
            return result.Content;
        }

        public async Task<TResponse> PostAsync<TRequest, TResponse>(string uri, TRequest data, bool bodyAsRestParameter = false, Dictionary<string, string> queryParameters = null, Dictionary<string, object> parameters = null) where TResponse : class, new()
        {
            var request = new RestRequest(uri.TrimStart('/'), Method.POST);
            SetParameters(request, parameters);
            SetQueryParameters(request, queryParameters);
            AddJson(request, bodyAsRestParameter, data);
            return await ExecuteAsync<TResponse>(request);
        }

        public async Task<IRestResponse> PostAsync<TRequest>(string uri, TRequest data, bool bodyAsRestParameter = false, Dictionary<string, string> queryParameters = null, Dictionary<string, object> parameters = null)
        {
            var request = new RestRequest(uri.TrimStart('/'), Method.POST);
            SetParameters(request, parameters);
            SetQueryParameters(request, queryParameters);
            AddJson(request, bodyAsRestParameter, data);
            return await ExecuteRequestAsync(request);
        }

        public async Task<TResponse> PatchAsync<TRequest, TResponse>(string uri, TRequest data, bool bodyAsRestParameter = false, Dictionary<string, string> queryParameters = null, Dictionary<string, object> parameters = null) where TResponse : class, new()
        {
            var request = new RestRequest(uri.TrimStart('/'), Method.PATCH);
            SetParameters(request, parameters);
            SetQueryParameters(request, queryParameters);
            AddJson(request, bodyAsRestParameter, data);
            return await ExecuteAsync<TResponse>(request);
        }

        public async Task<IRestResponse> PatchAsync<TRequest>(string uri, TRequest data, bool bodyAsRestParameter = false, Dictionary<string, string> queryParameters = null, Dictionary<string, object> parameters = null)
        {
            var request = new RestRequest(uri.TrimStart('/'), Method.PATCH);
            SetParameters(request, parameters);
            SetQueryParameters(request, queryParameters);
            AddJson(request, bodyAsRestParameter, data);
            return await ExecuteRequestAsync(request);
        }

        public async Task<TResponse> PutAsync<TRequest, TResponse>(string uri, TRequest data, bool bodyAsRestParameter = false, Dictionary<string, string> queryParameters = null, Dictionary<string, object> parameters = null) where TResponse : class, new()
        {
            var request = new RestRequest(uri.TrimStart('/'), Method.PUT);
            SetParameters(request, parameters);
            SetQueryParameters(request, queryParameters);
            AddJson(request, bodyAsRestParameter, data);
            return await ExecuteAsync<TResponse>(request);
        }

        public async Task<IRestResponse> PutAsync<TRequest>(string uri, TRequest data, bool bodyAsRestParameter = false, Dictionary<string, string> queryParameters = null, Dictionary<string, object> parameters = null)
        {
            var request = new RestRequest(uri.TrimStart('/'), Method.PUT);
            SetParameters(request, parameters);
            SetQueryParameters(request, queryParameters);
            AddJson(request, bodyAsRestParameter, data);
            return await ExecuteRequestAsync(request);
        }

        public async Task<TResponse> PostAsync<TResponse>(string uri, List<FileDetail> fileDetails, Dictionary<string, string> queryParameters = null, Dictionary<string, object> parameters = null) where TResponse : class, new()
        {
            if (fileDetails == null || !fileDetails.Any())
                throw new ArgumentNullException(nameof(fileDetails));

            var request = new RestRequest(uri.TrimStart('/'), Method.POST);
            SetParameters(request, parameters);
            SetQueryParameters(request, queryParameters);
            foreach (var file in fileDetails)
            {
                request.AddFile(file.Name, file.FileBytes, file.FileName);
            }
            return await ExecuteAsync<TResponse>(request);
        }

        private void SetQueryParameters(IRestRequest request, Dictionary<string, string> queryParameters)
        {
            if (queryParameters != null)
            {
                foreach (var queryParameter in queryParameters)
                {
                    request.AddQueryParameter(queryParameter.Key, queryParameter.Value);
                }
            }
        }

        private void SetParameters(IRestRequest request, Dictionary<string, object> parameters)
        {
            if (parameters != null)
            {
                foreach (var parameter in parameters)
                {
                    request.AddParameter(parameter.Key, parameter.Value);
                }
            }
        }

        private void AddJson<TRequest>(IRestRequest request, bool bodyAsRestParameter, TRequest data)
        {
            if (bodyAsRestParameter)
            {
                var json = JsonConvert.SerializeObject(data);
                request.AddParameter("application/json", json, ParameterType.RequestBody);
            }
            else
            {
                request.AddJsonBody(data);
            }
        }

    }
}