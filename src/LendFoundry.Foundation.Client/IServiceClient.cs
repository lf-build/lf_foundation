using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace LendFoundry.Foundation.Client
{
    public interface IServiceClient
    {
        T Execute<T>(IRestRequest request);

        bool Execute(IRestRequest request);

        object Execute(IRestRequest request, Type type);

        IRestResponse ExecuteRequest(IRestRequest request);

        Task<T> ExecuteAsync<T>(IRestRequest request);

        Task<bool> ExecuteAsync(IRestRequest request);

        Task<object> ExecuteAsync(IRestRequest request, Type type);

        Task<IRestResponse> ExecuteRequestAsync(IRestRequest request);

        Task<T> GetAsync<T>(string uri, Dictionary<string, string> queryParameters = null) where T : class, new();

        Task DeleteAsync(string uri, Dictionary<string, string> queryParameters = null);
        Task DeleteAsync<TRequest>(string uri, TRequest data, bool bodyAsRestParameter = false, Dictionary<string, string> queryParameters = null);

        Task<byte[]> GetByteArrayAsync(string uri, Dictionary<string, string> queryParameters = null);

        Task<Stream> GetStreamAsync(string uri, Dictionary<string, string> queryParameters = null);

        Task<string> GetStringAsync(string uri, Dictionary<string, string> queryParameters = null);

        Task<TResponse> PostAsync<TRequest, TResponse>(string uri, TRequest data, bool bodyAsRestParameter = false, Dictionary<string, string> queryParameters = null, Dictionary<string, object> parameters = null) where TResponse : class, new();

        Task<IRestResponse> PostAsync<TRequest>(string uri, TRequest data, bool bodyAsRestParameter = false, Dictionary<string, string> queryParameters = null, Dictionary<string, object> parameters = null);

        Task<TResponse> PutAsync<TRequest, TResponse>(string uri, TRequest data, bool bodyAsRestParameter = false, Dictionary<string, string> queryParameters = null, Dictionary<string, object> parameters = null) where TResponse : class, new();

        Task<IRestResponse> PutAsync<TRequest>(string uri, TRequest data, bool bodyAsRestParameter = false, Dictionary<string, string> queryParameters = null, Dictionary<string, object> parameters = null);

        Task<TResponse> PostAsync<TResponse>(string uri, List<FileDetail> fileDetails, Dictionary<string, string> queryParameters = null, Dictionary<string, object> parameters = null) where TResponse : class, new();

        Task<TResponse> PatchAsync<TRequest, TResponse>(string uri, TRequest data, bool bodyAsRestParameter = false, Dictionary<string, string> queryParameters = null, Dictionary<string, object> parameters = null) where TResponse : class, new();
        Task<IRestResponse> PatchAsync<TRequest>(string uri, TRequest data, bool bodyAsRestParameter = false, Dictionary<string, string> queryParameters = null, Dictionary<string, object> parameters = null);
    }
}