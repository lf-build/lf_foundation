using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace LendFoundry.Foundation.Client
{
    public class InterfaceListConverter<TInterface, TConcrete> : JsonConverter where TConcrete : class, TInterface
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(TInterface);
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            serializer.Serialize(writer, value, typeof(TConcrete));
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var res= serializer.Deserialize<List<TConcrete>>(reader);
            return res?.ConvertAll(x => (TInterface) x);
        }
    }
}