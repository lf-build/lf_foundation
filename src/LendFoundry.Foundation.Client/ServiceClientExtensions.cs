﻿using System;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using System.Linq;
using System.Collections.Concurrent;
#if DOTNET2
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.AspNet.Http;
using Microsoft.Framework.DependencyInjection;
#endif

namespace LendFoundry.Foundation.Client
{
    public static class ServiceClientExtensions
    {

        public static IServiceClient GetServiceClient(this IServiceCollection services, IServiceProvider provider, string endpoint, int port)
        {
            return provider.GetServiceClient(endpoint, port);
        }

        public static IServiceClient GetServiceClient(this IServiceCollection services, IServiceProvider provider, Uri uri)
        {
            return provider.GetServiceClient(uri);
        }

        public static IServiceClient GetServiceClient(this IServiceProvider provider, string endpoint, int port)
        {
            var reader = provider.GetRequiredService<ITokenReader>();
            return provider.GetServiceClient(reader, endpoint, port);
        }

        public static IServiceClient GetServiceClient(this IServiceProvider provider, Uri uri)
        {
            var reader = provider.GetRequiredService<ITokenReader>();
            return provider.GetServiceClient(reader, uri);
        }

        public static IServiceClient GetServiceClient(this IServiceProvider provider, ITokenReader reader, string endpoint, int port)
        {
            return provider.GetServiceClient(reader, new UriBuilder("http", endpoint, port).Uri);
        }

        public static IServiceClient GetServiceClient(this IServiceProvider provider, ITokenReader reader, Uri uri)
        {
            var accessor = provider.GetRequiredService<IHttpContextAccessor>();
            var logger = provider.GetRequiredService<ILogger>();
            return new ServiceClient(accessor, logger, reader, uri);
        }


        public static bool Contains<T>(this IServiceCollection services)
        {
            return services.Any(d => d.ServiceType == typeof(T));
        }
    }
}