namespace LendFoundry.Foundation.Client
{
    public class Error
    {
        public Error()
        {
        }

        public Error(string message)
        {
            Message = message;
        }

        public Error(int code, string message) : this(message)
        {
            Code = code;
        }

        public int Code { get; set; }
        public string Message { get; set; }
    }
}