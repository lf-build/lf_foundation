﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;

namespace LendFoundry.Foundation.Client
{
    public interface IDependencyServiceUriResolverFactory
    {
        IDependencyServiceUriResolver Create(ITokenReader reader, ILogger logger);
    }



}
