﻿using System;

namespace LendFoundry.Foundation.Client
{
    public interface IDependencyServiceUriResolver
    {
        Uri Get(string serviceName);
    }
    
}
