﻿using Newtonsoft.Json;
using System;

namespace LendFoundry.Foundation.Client
{
    public class InterfaceConverter<TInterface, TConcrete> : JsonConverter where TConcrete : class, TInterface
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(TInterface);
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            serializer.Serialize(writer, value, typeof(TConcrete));
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            return serializer.Deserialize<TConcrete>(reader);
        }
    }
}