﻿using System.Collections.Generic;

namespace LendFoundry.Foundation.Client
{
    public interface IDependencyConfiguration
    {
        Dictionary<string, string> Dependencies { get; set; }
        string Database { get; set; }
    }

}
