﻿using LendFoundry.Foundation.Client;
using System.Collections.Generic;

namespace LendFoundry.Foundation.ServiceDependencyResolver
{
    public class DependencyConfiguration : IDependencyConfiguration
    {
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
    }

}
