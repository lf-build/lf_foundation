﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Client;
using System;
using System.Collections.Generic;

namespace LendFoundry.Foundation.ServiceDependencyResolver
{
    public class DependencyServiceUriResolver : IDependencyServiceUriResolver
    {
        public DependencyServiceUriResolver(Dictionary<string, Uri> dependencyUris, ILogger logger)
        {
            DependencyUrls = dependencyUris;
            Logger = logger;
        }

        private Dictionary<string, Uri> DependencyUrls { get; }
        private ILogger Logger { get; }

        public Uri Get(string serviceName)
        {
            Uri uri;
            var urlFromEnv=Environment.GetEnvironmentVariable($"{serviceName.ToUpper()}_URL");
            if (!String.IsNullOrWhiteSpace(urlFromEnv))
            {
                return new Uri(urlFromEnv);
            }

            if (DependencyUrls.TryGetValue(serviceName, out uri))
            {
                return uri;
            }
            else
            {
                Logger.Warn($"Unable to resolve Service URL for {serviceName} using default {serviceName}:5000");
                return new UriBuilder("http", serviceName, 5000).Uri;
            }
        }
    }

}
