﻿using System;
using LendFoundry.Foundation.Client;
using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
namespace LendFoundry.Foundation.ServiceDependencyResolver
{
    public static class DependencyServiceUriResolverExtensions
    {
        public static IServiceCollection AddDependencyServiceUriResolver<TConfiguration>(this IServiceCollection services, string serviceName) where TConfiguration : class, IDependencyConfiguration, new()
        {
            services.AddTransient<IDependencyServiceUriResolverFactory>(p => new DependencyServiceUriResolverFactory<TConfiguration>(p, serviceName));
            return services;
        }
        
    }
}

