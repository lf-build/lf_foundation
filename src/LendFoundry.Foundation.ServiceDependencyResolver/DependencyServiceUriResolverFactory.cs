﻿using LendFoundry.Configuration;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Client;
using LendFoundry.Security.Tokens;
using System;
using System.Collections.Generic;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif


namespace LendFoundry.Foundation.ServiceDependencyResolver
{
    internal class DependencyServiceUriResolverFactory<T> : IDependencyServiceUriResolverFactory where T : class, IDependencyConfiguration, new()
    {
        private IServiceProvider Provider { get; }
        private string ServiceName { get; }

        public DependencyServiceUriResolverFactory(IServiceProvider provider, string serviceName)
        {
            Provider = provider;
            ServiceName = serviceName;
        }

        public IDependencyServiceUriResolver Create(ITokenReader reader, ILogger logger)
        {
            var configurationFactory = Provider.GetService<IConfigurationServiceFactory>();
            var defaultConfiguration = configurationFactory.Create<DependencyConfiguration>("tenant", reader).Get();
            var serviceconfiguration = configurationFactory.Create<T>(ServiceName, reader).Get();
            var dependencyUrls = new Dictionary<string, Uri>(StringComparer.InvariantCultureIgnoreCase);
            if (defaultConfiguration != null && defaultConfiguration.Dependencies!=null)
            {
                foreach (var item in defaultConfiguration.Dependencies)
                {
                    dependencyUrls.Add(item.Key, new Uri(item.Value));
                }
            }
            if (serviceconfiguration != null && serviceconfiguration.Dependencies!=null)
            {
                foreach (var item in serviceconfiguration.Dependencies)
                {
                    dependencyUrls[item.Key]= new Uri(item.Value);
                }
            }

            return new DependencyServiceUriResolver(dependencyUrls, logger);
        }

    }

}