﻿using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Routing;
using Microsoft.AspNet.Routing.Template;
using Newtonsoft.Json;
using Swashbuckle.Application;
using Swashbuckle.Swagger;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace LendFoundry.Foundation.Documentation
{
    public class SwaggerDocumentationMiddleware
    {
        private readonly RequestDelegate next;
        private readonly ISwaggerProvider swaggerProvider;
        private readonly TemplateMatcher requestMatcher;
        private readonly JsonSerializer swaggerSerializer;

        public SwaggerDocumentationMiddleware(RequestDelegate next, ISwaggerProvider swaggerProvider, string routeTemplate)
        {
            this.next = next;
            this.swaggerProvider = swaggerProvider;
            requestMatcher = new TemplateMatcher(TemplateParser.Parse(routeTemplate), new RouteValueDictionary());
            swaggerSerializer = new JsonSerializer
            {
                NullValueHandling = NullValueHandling.Ignore,
                ContractResolver = new SwaggerDocsContractResolver()
            }; ;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            string apiVersion;
            if (!RequestingSwaggerDocs(httpContext.Request, out apiVersion))
                await next.Invoke(httpContext);
            else
                RespondWithSwaggerJson(httpContext.Response, swaggerProvider.GetSwagger(apiVersion, null, PathString.FromUriComponent(httpContext.Request.PathBase), null));
        }

        private bool RequestingSwaggerDocs(HttpRequest request, out string apiVersion)
        {
            apiVersion = null;
            if (request.Method != "GET")
                return false;

            TemplateMatcher requestMatcher = this.requestMatcher;
            PathString path = request.Path;

            // ISSUE: explicit reference operation
            string str = @path.ToUriComponent().Trim('/');
            IDictionary<string, object> dictionary = requestMatcher.Match(str);
            if (dictionary == null || !dictionary.ContainsKey("apiVersion"))
                return false;

            apiVersion = dictionary["apiVersion"].ToString();
            return true;
        }

        private void RespondWithSwaggerJson(HttpResponse response, SwaggerDocument swagger)
        {
            response.StatusCode = 200;
            response.ContentType = "application/json";

            using (StreamWriter streamWriter = new StreamWriter(response.Body))
            {
                swaggerSerializer.Serialize(streamWriter, swagger);
            }
        }
    }
}