﻿using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Routing;
using Microsoft.AspNet.Routing.Template;
using Swashbuckle.Application;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace LendFoundry.Foundation.Documentation
{
    public class SwaggerDocumentationUiMiddleware
    {
        private readonly RequestDelegate next;
        private readonly SwaggerPathHelper swaggerPathHelper;
        private readonly TemplateMatcher requestMatcher;
        private readonly Assembly resourceAssembly;

        public SwaggerDocumentationUiMiddleware
        (
            RequestDelegate next,
            SwaggerPathHelper swaggerPathHelper,
            string routeTemplate
        )
        {
            this.next = next;
            this.swaggerPathHelper = swaggerPathHelper;

            requestMatcher = new TemplateMatcher(TemplateParser.Parse(routeTemplate), new RouteValueDictionary());
            resourceAssembly = GetType().GetTypeInfo().Assembly;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            if (!RequestingSwaggerUi(httpContext.Request))
            {
                await next(httpContext);
                return;
            }

            //var template = resourceAssembly.GetManifestResourceStream($"{Assembly.GetExecutingAssembly().GetName().Name}.SwaggerUi.index.html");
            var template = resourceAssembly.GetManifestResourceStream($"{Assembly.GetExecutingAssembly().GetName().Name}.bower_components.swagger_ui.dist.index.html");
            var content = AssignPlaceholderValuesTo(template, httpContext);
            RespondWithContentHtml(httpContext.Response, content);
        }

        private bool RequestingSwaggerUi(HttpRequest request)
        {
            if (request.Method != "GET") return false;

            var routeValues = requestMatcher.Match(request.Path.ToUriComponent().Trim('/'));

            return (routeValues != null);
        }

        private Stream AssignPlaceholderValuesTo(Stream template, HttpContext httpContext)
        {
            var basePath = httpContext.Request.PathBase;
            var swaggerPath = swaggerPathHelper.GetPathDescriptors(basePath).First().Path;

            var placeholderValues = new Dictionary<string, string>
            {
                { "%(SwaggerUrl)", swaggerPath }
            };

            var templateText = new StreamReader(template).ReadToEnd();
            var contentBuilder = new StringBuilder(templateText);
            foreach (var entry in placeholderValues)
            {
                contentBuilder.Replace(entry.Key, entry.Value);
            }

            return new MemoryStream(Encoding.UTF8.GetBytes(contentBuilder.ToString()));
        }

        private void RespondWithContentHtml(HttpResponse response, Stream content)
        {
            response.StatusCode = 200;
            response.ContentType = "text/html";
            content.CopyTo(response.Body);
        }
    }
}