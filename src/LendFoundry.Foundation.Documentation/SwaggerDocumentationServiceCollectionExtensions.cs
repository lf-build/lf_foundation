﻿using Microsoft.Framework.DependencyInjection;
using Swashbuckle.Swagger;
using Swashbuckle.Swagger.XmlComments;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace LendFoundry.Foundation.Documentation
{
    public static class SwaggerDocumentationServiceCollectionExtensions
    {
        /// <summary>
        /// Add the Swagger Documentation.
        /// </summary>
        /// <param name="services">The services.</param>
        /// <param name="serviceName">Name of the Service.</param>
        /// <param name="description">The description.(Optional)</param>
        /// <returns>IServiceCollection</returns>
        public static IServiceCollection AddSwaggerDocumentation(this IServiceCollection services, string description = null)
        {
            var currentRootPath = Environment.CurrentDirectory;
            var currentAssemblyName = currentRootPath.Split('\\').Last();
            var basePath = Path.GetFullPath(Path.Combine(currentRootPath, @"..\..\"));

            services.AddSwagger();
            services.ConfigureSwaggerSchema(options =>
            {
                options.DescribeAllEnumsAsStrings = true;
                options.DescribeStringEnumsInCamelCase = false;
                options.IgnoreObsoleteProperties = true;
            });

            services.ConfigureSwaggerDocument(options =>
            {
                options.IgnoreObsoleteActions = true;

                options.SingleApiVersion(new Info
                {
                    Title = ExtractServiceName(currentAssemblyName),
                    Version = "docs"
                });

                if (!options.SecurityDefinitions.Any(s => s.Key.Equals("apiKey", StringComparison.InvariantCultureIgnoreCase)))
                {
                    options.SecurityDefinitions.Add("apiKey", new ApiKeyScheme
                    {
                        Type = "apiKey",
                        Name = "Authorization",
                        Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                        In = "header"
                    });
                }

                // Search for the comment XML file
                var latestXmlFile = GetLatestXmlFile(currentRootPath);
                if (!string.IsNullOrWhiteSpace(latestXmlFile))
                {
                    options.OperationFilter(new ApplyXmlActionComments(latestXmlFile));
                }
            });
            return services;
        }

        /// <summary>
        /// Extracts the name of the service.
        /// </summary>
        /// <param name="assemblyName">Name of the assembly.</param>
        /// <returns>E.g. "Application.Filters"</returns>
        private static string ExtractServiceName(string assemblyName)
        {
            var result = new List<string>();

            // Check if OS is Linux or MacOSX
            if (new[] { PlatformID.Unix, PlatformID.MacOSX }.Any(os => os.Equals(Environment.OSVersion.Platform)))
            {
                // Linux or MacOSX Approach
                result = assemblyName.Split('.')
                .Where(n => n != "/app/LendFoundry" && n != "Api")
                .Select(n => { return n; }).ToList();
            }
            else
            {
                // Windows Approach
                result = assemblyName.Split('.')
                .Where(n => n != "LendFoundry" && n != "Api")
                .Select(n => { return n; }).ToList();
            }

            if (result.Count() > 1)
                return string.Join(".", result);
            else
                return result.FirstOrDefault();
        }

        private static string GetLatestXmlFile(string basePath)
        {
            // Check if OS is Linux or MacOSX
            if (new[] { PlatformID.Unix, PlatformID.MacOSX }.Any(os => os.Equals(Environment.OSVersion.Platform)))
            {
                // Linux or MacOSX Approach
                var xmlFilesFound = Directory.GetFiles(basePath, "*.xml", SearchOption.AllDirectories);
                if (xmlFilesFound != null)
                {
                    var latestXmlFile = xmlFilesFound.OrderByDescending(f => File.GetLastWriteTimeUtc(f)).FirstOrDefault();
                    if (latestXmlFile != null)
                    {
                        return latestXmlFile;
                    }
                }
            }
            else
            {
                // Windows Approach
                var xmlFilesFound = Directory.GetFiles(Path.GetFullPath(Path.Combine(basePath, @"..\..\")), $"*{basePath.Split('\\').Last()}.xml", SearchOption.AllDirectories);
                if (xmlFilesFound != null)
                {
                    var latestXmlFile = xmlFilesFound.OrderByDescending(f => File.GetLastWriteTimeUtc(f)).FirstOrDefault();
                    if (latestXmlFile != null)
                    {
                        return latestXmlFile;
                    }
                }
            }

            return string.Empty;
        }
    }
}