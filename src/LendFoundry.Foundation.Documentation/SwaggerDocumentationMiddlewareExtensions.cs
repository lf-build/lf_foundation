﻿using Microsoft.AspNet.Builder;
using System;
using System.IO;
using System.Linq;
using System.Reflection;

namespace LendFoundry.Foundation.Documentation
{
    public static class SwaggerDocumentationMiddlewareExtensions
    {
        public static IApplicationBuilder UseSwaggerDocumentation(this IApplicationBuilder app)
        {
            ExtractAssemblies();

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger("{apiVersion}/swagger.json");

            // Enable middleware to serve swagger-ui (HTML, JS, CSS etc.), specifying the Swagger JSON endpoint.
            //app.UseSwaggerDocumentationUi();

            //return app.UseMiddleware<SwaggerDocumentationMiddleware>();
            return app;
        }

        private static void ExtractAssemblies()
        {
            var currentRootPath = Environment.CurrentDirectory;
            var currentAssemblyName = currentRootPath.Split('\\').Last();
            var basePath = Path.GetFullPath(Path.Combine(currentRootPath, @"..\..\"));
            var baseDirectory = Path.Combine(basePath, @"artifacts\bin\" + currentAssemblyName);

            // Check if OS is Linux or MacOSX
            if (new [] { PlatformID.Unix, PlatformID.MacOSX }.Any(os => os.Equals(Environment.OSVersion.Platform)))
            {
                // Linux or MacOSX Approach
                basePath = Path.GetFullPath(Path.Combine(currentRootPath, "bin/"));
                var xmlFilesFound = Directory.GetFiles(basePath, "*.xml", SearchOption.AllDirectories);
                if (xmlFilesFound != null)
                {
                    var latestXmlFile = xmlFilesFound.OrderByDescending(f => File.GetLastWriteTimeUtc(f)).FirstOrDefault();
                    if (latestXmlFile != null)
                    {
                        baseDirectory = Path.GetDirectoryName(latestXmlFile);
                        CopyAndLoadAssemblies(baseDirectory);
                    }
                }
            }
            else
            {
                // Windows Approach
                var xmlFilesFound = Directory.GetFiles(baseDirectory, $"*{currentAssemblyName}.xml", SearchOption.AllDirectories);
                if (xmlFilesFound != null)
                {
                    var latestXmlFile = xmlFilesFound.OrderByDescending(f => File.GetLastWriteTimeUtc(f)).FirstOrDefault();
                    if (latestXmlFile != null)
                    {
                        baseDirectory = Path.GetDirectoryName(latestXmlFile);
                        CopyAndLoadAssemblies(baseDirectory);
                    }                    
                }
            }
        }

        private static void CopyStream(Stream stream, string destPath)
        {
            using (var fileStream = new FileStream(destPath, FileMode.Create, FileAccess.Write))
            {
                stream.CopyTo(fileStream);
            }
        }

        private static void CopyAndLoadAssemblies(string baseDirectory)
        {
            var assemblies = new[] { "Swashbuckle.dll", "Swashbuckle.Swagger.dll" };
            var resourceAssembly = Assembly.GetAssembly(typeof(SwaggerDocumentationMiddlewareExtensions));

            foreach (var assemblyName in assemblies)
            {
                var assemblyPath = Path.Combine(baseDirectory, assemblyName);
                if (!Directory.Exists(baseDirectory))
                    Directory.CreateDirectory(baseDirectory);

                using (var stream = resourceAssembly.GetManifestResourceStream($"{resourceAssembly.GetName().Name}.Libraries.{assemblyName}"))
                {
                    CopyStream(stream, assemblyPath);
                }

                Assembly.LoadFile(assemblyPath);
            }
        }
    }
}