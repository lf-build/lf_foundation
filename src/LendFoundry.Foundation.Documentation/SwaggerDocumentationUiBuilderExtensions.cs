﻿using Microsoft.AspNet.Builder;
using Microsoft.AspNet.FileProviders;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.StaticFiles;
using Swashbuckle.Application;
using System;
using System.Reflection;

namespace LendFoundry.Foundation.Documentation
{
    public static class SwaggerDocumentationUiBuilderExtensions
    {
        public static void UseSwaggerDocumentationUi(this IApplicationBuilder app, string basePath = "swagger/ui")
        {
            ThrowIfServicesNotRegistered(app.ApplicationServices);

            basePath = basePath.Trim('/');
            var indexPath = basePath + "/index.html";

            // Enable redirect from basePath to indexPath
            app.UseMiddleware<RedirectMiddleware>(basePath, indexPath);
            app.UseMiddleware<SwaggerDocumentationUiMiddleware>(indexPath);

            var options = new FileServerOptions
            {
                RequestPath = PathString.FromUriComponent("/" + basePath),
                FileProvider = new EmbeddedFileProvider
                (
                    typeof(SwaggerDocumentationUiBuilderExtensions).GetTypeInfo().Assembly, 
                    $"{Assembly.GetExecutingAssembly().GetName().Name}.bower_components.swagger_ui.dist"
                )
            };
            options.EnableDefaultFiles = false;
            options.StaticFileOptions.ContentTypeProvider = new FileExtensionContentTypeProvider();

            app.UseFileServer(options);
        }

        private static void ThrowIfServicesNotRegistered(IServiceProvider applicationServices)
        {
            var requiredServices = new[] { typeof(SwaggerPathHelper) };
            foreach (var type in requiredServices)
            {
                var service = applicationServices.GetService(type);
                if (service == null)
                {
                    throw new InvalidOperationException(string.Format("Required service '{0}' not registered - are you missing a call to AddSwagger?", type));
                }
            }
        }
    }
}