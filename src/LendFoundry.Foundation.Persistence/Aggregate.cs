using Newtonsoft.Json;

namespace LendFoundry.Foundation.Persistence
{
    public abstract class Aggregate : IAggregate
    {
        [JsonIgnore]
        public string TenantId { get; set; }

        public virtual string Id { get; set; }
    }
}