using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace LendFoundry.Foundation.Persistence
{
    public interface IRepository<TAggregate> where TAggregate : IAggregate
    {
        Task<TAggregate> Get(string id);

        Task<IEnumerable<TAggregate>> All(Expression<Func<TAggregate, bool>> query, int? skip = null, int? quantity = null);

        void Add(TAggregate item);

        void Remove(TAggregate item);

        void Update(TAggregate item);

        int Count(Expression<Func<TAggregate, bool>> query);
    }
}