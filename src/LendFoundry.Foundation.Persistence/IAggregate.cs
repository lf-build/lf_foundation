﻿namespace LendFoundry.Foundation.Persistence
{
    public interface IAggregate
    {
        string TenantId { get; set; }

        string Id { get; set; }
    }
}