﻿namespace LendFoundry.Foundation.Listener
{
    public class ConfigurationEvent
    {
        public string Service { get; set; }
        public object Value { get; set; }
    }
}
