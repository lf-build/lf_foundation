﻿using LendFoundry.Configuration;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Abstractions;
using LendFoundry.Tenant.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Foundation.Listener
{
    public abstract class ListenerBase
    {
        public ListenerBase(
    ITokenHandler tokenHandler,
    IEventHubClientFactory eventHubFactory,
    ILoggerFactory loggerFactory,
    ITenantServiceFactory tenantServiceFactory,
    string serviceName
)
        {
            if (tokenHandler == null)
                throw new ArgumentException($"{nameof(tokenHandler)} is mandatory");

            if (eventHubFactory == null)
                throw new ArgumentException($"{nameof(eventHubFactory)} is mandatory");

            if (loggerFactory == null)
                throw new ArgumentException($"{nameof(loggerFactory)} is mandatory");

            if (tenantServiceFactory == null)
                throw new ArgumentException($"{nameof(tenantServiceFactory)} is mandatory");

            TokenHandler = tokenHandler;
            EventHubFactory = eventHubFactory;
            LoggerFactory = loggerFactory;
            TenantServiceFactory = tenantServiceFactory;
            ServiceName = serviceName;
        }

        private String ServiceName { get; }

        private ITokenHandler TokenHandler { get; }

        private IEventHubClientFactory EventHubFactory { get; }

        private ILoggerFactory LoggerFactory { get; }

        private ITenantServiceFactory TenantServiceFactory { get; }

        private ConcurrentDictionary<string, List<string>> Events { get; set; }

        public void Start()
        {
            var logger = LoggerFactory.Create(NullLogContext.Instance);
            logger.Info("Starting Listener...");

            try
            {
                var emptyReader = new StaticTokenReader(string.Empty);
                var tenantService = TenantServiceFactory.Create(emptyReader);
                var tenants = tenantService.GetActiveTenants();
                logger.Info($"#{tenants.Count} active tenant(s) found to be processed");

                if (Events == null)
                    Events = new ConcurrentDictionary<string, List<string>>();

                // Tenant add, update, activate and deactivate auto detection handling.
                var eventhubWithoutTenant = EventHubFactory.Create(emptyReader);
                var tenantEvents = new List<string> { "TenantAdded", "TenantActivated", "TenantDeactivated", "TenantUpdated" };
                tenantEvents.ForEach(tenantEventName =>
                {
                    eventhubWithoutTenant.OnAll(tenantEventName, (@event) =>
                    {
                        logger.Info($"Processing {@event.Name} with Id - {@event.Id} ");

                        var data = JsonConvert.DeserializeObject<TenantEvent>(@event.Data.ToString());
                        var tenant = data.Tenant;
                        logger.Info($"Processing tenant #{tenant.Id}");
                        bool isSubscribe = true;
                        switch (tenantEventName)
                        {
                            case "TenantAdded":
                            case "TenantUpdated":
                                if (!tenant.IsActive)
                                    isSubscribe = false;
                                break;
                            case "TenantDeactivated":
                                isSubscribe = false;
                                break;
                        }
                        if (isSubscribe)
                        {
                            var uniqueEvents = SubscribeEvents(tenant.Id, logger);
                            Events.AddOrUpdate(tenant.Id, uniqueEvents, (a, b) => uniqueEvents);
                        }
                        else
                        {
                            List<string> events = null;
                            Events.TryGetValue(tenant.Id, out events);
                            if (events != null)
                            {
                                foreach (var eventName in events)
                                {
                                    eventhubWithoutTenant.Unsubscribe(tenant.Id, eventName);
                                }
                            }
                            Events.TryRemove(tenant.Id, out events);
                        }
                    });
                });

                // Configuration add, update and delete auto detection handling.
                var configurationEvents = new List<string> { "ConfigurationUpdated", "ConfigurationRemoved" };
                configurationEvents.ForEach(configurationEventName =>
                {
                    eventhubWithoutTenant.OnAll(configurationEventName, (@event) =>
                    {
                        logger.Info($"Processing {@event.Name} with Id - {@event.Id} ");

                        var data = JsonConvert.DeserializeObject<ConfigurationEvent>(@event.Data.ToString());
                        if (data.Service == ServiceName)
                        {
                            var token = TokenHandler.Issue(@event.TenantId, ServiceName);
                            var reader = new StaticTokenReader(token.Value);
                            var eventNames = GetEventNames(@event.TenantId);

                            if (eventNames == null || !eventNames.Any())
                            {
                                List<string> events = null;
                                Events.TryGetValue(@event.TenantId, out events);
                                if (events != null)
                                {
                                    foreach (var eventName in events)
                                    {
                                        eventhubWithoutTenant.Unsubscribe(@event.TenantId, eventName);
                                    }
                                }
                                Events.TryRemove(@event.TenantId, out events);
                            }
                            else
                            {
                                if (configurationEventName == "ConfigurationUpdated")
                                {
                                    List<string> events = null;
                                    Events.TryGetValue(@event.TenantId, out events);
                                    if (events != null)
                                    {
                                        foreach (var eventName in events)
                                        {
                                            eventhubWithoutTenant.Unsubscribe(@event.TenantId, eventName);
                                        }
                                    }
                                    var uniqueEvents = SubscribeEvents(@event.TenantId, logger);
                                    Events.AddOrUpdate(@event.TenantId, uniqueEvents, (a,b) => uniqueEvents);
                                }
                            }
                        }
                    });
                });


                // Get all active tenants in place and set an eventhub subscription for each one.
                tenants.ForEach(tenant =>
                {
                    try
                    {
                        logger.Info($"Processing tenant #{tenant.Id}");
                        var uniqueEvents = SubscribeEvents(tenant.Id, logger);
                        Events.AddOrUpdate(tenant.Id, uniqueEvents, (a, b) => uniqueEvents);
                    }
                    catch (Exception ex)
                    {
                        logger.Error($"Error while listening eventhub to process for tenant {tenant.Id}", ex);
                    }
                });
                logger.Info("listener started");
            }
            catch (Exception ex)
            {
                logger.Error($"Error while listening eventhub to process {ServiceName}", ex);
                logger.Info($"\n{ServiceName} is working yet and waiting new event\n");
            }
        }

        public abstract List<string> SubscribeEvents(string tenant, ILogger logger);

        public abstract List<string> GetEventNames(string tenant);

    }
}
