﻿using System;
using System.Collections.Generic;

namespace LendFoundry.Foundation.Testing
{
    internal class Table
    {
        public Table(TableRow header)
        {
            Header = header;
        }

        public TableRow Header { get; }
        public IList<TableRow> Body { get; } = new List<TableRow>();

        internal void Add(int index, string[] values)
        {
            var rowNumber = index - Header.Index;

            if (values.Length != Header.Values.Length)
                throw new Exception("Number of values in row #" + rowNumber + " does not match the number of property names in the table's header");

            Body.Add(new TableRow { Index = index, Number = rowNumber, Values = values });
        }
    }

    internal class TableRow
    {
        public int Index { get; set; }
        public int Number { get; set; }
        public string[] Values { get; set; }
    }
}
