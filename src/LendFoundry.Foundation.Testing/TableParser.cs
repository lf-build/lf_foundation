﻿using LendFoundry.Foundation.Date;
using System;

namespace LendFoundry.Foundation.Testing
{
    internal class TableParser
    {
        private static char[] RowDelimiter = { ';', '\n' };
        private const char ColumnDelimiter = '|';
        private static ITenantTime TenantTime = new UtcTenantTime();

        public static Table Parse(string table)
        {
            var rows = table.Split(RowDelimiter);

            if (rows.Length == 0)
                throw new ArgumentException("Table is empty");

            var header = GetTableHeader(rows);
            var result = new Table(header);
            var startIndex = header.Index + 1;

            for (int index = startIndex; index < rows.Length; index++)
            {
                var row = rows[index];
                if (!ShouldIgnoreRow(row))
                    result.Add(index, SplitColumns(row));
            }

            return result;
        }

        private static TableRow GetTableHeader(string[] rows)
        {
            for (var index = 0; index < rows.Length; index++)
            {
                var row = rows[index];
                if (!string.IsNullOrWhiteSpace(row) && !ShouldIgnoreRow(row))
                    return new TableRow { Index = index, Values = SplitColumns(row) };
            }
            throw new ArgumentException("Table header not found");
        }

        private static bool ShouldIgnoreRow(string row)
        {
            return !row.Contains("|");
        }

        private static string[] SplitColumns(string row)
        {
            return TrimSpaces(row.Split(ColumnDelimiter));
        }

        private static string[] TrimSpaces(string[] values)
        {
            for (int index = 0; index < values.Length; index++)
                values[index] = values[index].Trim();
            return values;
        }
    }
}
