﻿using LendFoundry.Foundation.Date;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using Xunit;
using Xunit.Sdk;

namespace LendFoundry.Foundation.Testing
{
    public class AssertTable
    {
        private static readonly string ItemNumberProperty = "#";

        private AssertTable(string table)
        {
            ExpectedTable = TableParser.Parse(table);
            PropertyNames = ExpectedTable.Header.Values;
        }

        public static void Equal<I>(string expectedTableOfItems, IEnumerable<I> actualItems)
        {
            new AssertTable(expectedTableOfItems).Check(actualItems.ToList());
        }

        private Table ExpectedTable { get; }
        private string[] PropertyNames { get; }

        private void Check(IList items)
        {
            if (items == null)
                throw new ArgumentNullException("List cannot be null");

            if (items.Count != ExpectedTable.Body.Count)
                throw new ArgumentException($"Expected {ExpectedTable.Body.Count} items, got {items.Count} instead");

            for (int index = 0; index < ExpectedTable.Body.Count; index++)
            {
                var item = items[index];
                var itemNumber = index + 1;
                var expectedValues = ExpectedTable.Body[index].Values;
                CheckProperties(item, itemNumber, expectedValues);
            }
        }

        private void CheckProperties(object item, int itemNumber, string[] expectedValues)
        {
            for (int propertyIndex = 0; propertyIndex < PropertyNames.Length; propertyIndex++)
            {
                string propertyName = PropertyNames[propertyIndex];

                if (propertyName != ItemNumberProperty)
                {
                    string expectedValue = expectedValues[propertyIndex];
                    if (propertyName != ItemNumberProperty)
                        CheckProperty(item, itemNumber, propertyName, expectedValue);
                }
            }
        }

        private void CheckProperty(object obj, int itemNumber, string propertyName, string expectedValueAsString)
        {
            Type type = obj.GetType();

            if (IsNestedProperty(propertyName))
            {
                CheckNestedProperty(obj, itemNumber, propertyName, expectedValueAsString, type);
            }
            else
            {
                var property = GetProperty(propertyName, type);
                CheckPropertyValue(property, expectedValueAsString, obj, itemNumber, propertyName);
            }
        }

        private void CheckNestedProperty(object obj, int itemNumber, string nestedPropertyName, string expectedValueAsString, Type type)
        {
            var names = nestedPropertyName.Split('.');

            var parentPropertyName = names[0];
            var parentProperty = GetProperty(parentPropertyName, type);
            var parentPropertyValue = parentProperty.GetValue(obj);

            var childPropertyName = names[1];
            var childProperty = GetProperty(childPropertyName, parentProperty.PropertyType);

            if (string.IsNullOrWhiteSpace(expectedValueAsString))
            {
                if (parentPropertyValue != null)
                    AssertChildPropertyIsNullOrDefault(itemNumber, nestedPropertyName, parentPropertyValue, childProperty);
            }
            else
            {
                if (parentPropertyValue == null)
                    throw new XunitException($"Expected not to be null: list[{itemNumber}].{parentPropertyName}");

                CheckPropertyValue(childProperty, expectedValueAsString, parentPropertyValue, itemNumber, nestedPropertyName);
            }
        }

        private void AssertChildPropertyIsNullOrDefault(int itemNumber, string nestedPropertyName, object parentPropertyValue, PropertyInfo childProperty)
        {
            var childPropertyValue = parentPropertyValue == null ? null : childProperty.GetValue(parentPropertyValue);
            var valueChecker = GetValueChecker(childProperty.PropertyType);
            var expectedValue = DefaultValueOf(childProperty.PropertyType);
            if (expectedValue == null)
            {
                if (childPropertyValue != null)
                    throw new AssertActualExpectedException(expectedValue, childPropertyValue, $"Unexpected value for list[{itemNumber}].{nestedPropertyName}");
            }
            else
            {
                if (!expectedValue.Equals(childPropertyValue))
                    throw new AssertActualExpectedException(expectedValue, childPropertyValue, $"Unexpected value for list[{itemNumber}].{nestedPropertyName}");
            }
        }

        private static bool IsNestedProperty(string propertyName)
        {
            return propertyName.Contains('.');
        }

        private object DefaultValueOf(Type type)
        {
            if (type.IsValueType) return Activator.CreateInstance(type);
            return null;
        }

        private void CheckPropertyValue(PropertyInfo property, string expectedValueAsString, object obj, int itemNumber, string propertyName)
        {
            var valueChecker = GetValueChecker(property.PropertyType);
            var expectedValue = valueChecker.ParseValue(expectedValueAsString);
            var actualValue = property.GetValue(obj);
            var areEqual = valueChecker.AreEqual(actualValue, expectedValue);
            if (!areEqual)
                throw new AssertActualExpectedException(expectedValue, actualValue, "Item #" + itemNumber + " has incorrect " + propertyName);
        }

        private static PropertyInfo GetProperty(string propertyName, Type type)
        {
            var property = type.GetProperty(propertyName);

            if (property == null)
                throw new ArgumentException($"Property '{propertyName}' does not exist in type {type}");

            return property;
        }

        private IValueChecker GetValueChecker(Type propertyType)
        {
            if (propertyType == typeof(string)) return new StringChecker();
            if (propertyType == typeof(int)) return new IntChecker();
            if (propertyType == typeof(double)) return new DoubleChecker();
            if (propertyType == typeof(DateTimeOffset) || propertyType == typeof(DateTimeOffset?)) return new DateOffsetChecker();
            if (propertyType.IsEnum) return new EnumChecker(propertyType);
            return new ObjectChecker();
            //throw new ArgumentException("No value checker for type " + propertyType);
        }
    }

    interface IValueChecker
    {
        object ParseValue(string valueText);
        bool AreEqual(object actualValue, object expectedValue);
    }

    class ObjectChecker : IValueChecker
    {
        public bool AreEqual(object actualValue, object expectedValue)
        {
            if (expectedValue == null)
                return actualValue == null;

            return expectedValue.Equals(actualValue);
        }

        public object ParseValue(string valueText)
        {
            if (string.IsNullOrWhiteSpace(valueText))
                return null;

            throw new Exception("Don't know how to parse to an object");
        }
    }

    class StringChecker : IValueChecker
    {
        public bool AreEqual(object actualValue, object expectedValue)
        {
            var expectedString = (string)expectedValue;
            var actualString = (string)actualValue;

            if (string.IsNullOrWhiteSpace(expectedString))
                return string.IsNullOrWhiteSpace(actualString);

            return actualString == expectedString;
        }

        public object ParseValue(string valueText) => valueText;
    }

    class DoubleChecker : IValueChecker
    {
        private const double Epsilon = 0.0000000001;

        public bool AreEqual(object actualValue, object expectedValue)
        {
            double actualNumber = (double)actualValue;
            double expectedNumber = (double)expectedValue;
            double delta = Math.Abs(expectedNumber - actualNumber);
            return delta <= Epsilon;
        }

        public object ParseValue(string valueText) => Double.Parse(valueText, CultureInfo.InvariantCulture.NumberFormat);
    }

    class IntChecker : IValueChecker
    {
        public bool AreEqual(object actualValue, object expectedValue)
        {
            int actualNumber = (int)actualValue;
            int expectedNumber = (int)expectedValue;
            return actualNumber == expectedNumber;
        }

        public object ParseValue(string valueText) => Int32.Parse(valueText, CultureInfo.InvariantCulture.NumberFormat);
    }

    class DateOffsetChecker : IValueChecker
    {
        private ITenantTime TenantTime = new UtcTenantTime();

        public bool AreEqual(object actualValue, object expectedValue)
        {
            if (actualValue == null)
                return null == expectedValue;

            var nullable = actualValue as DateTimeOffset?;
            if (nullable != null)
                return nullable.Equals(expectedValue);

            return ((DateTimeOffset)actualValue).Equals(expectedValue);
        }

        public object ParseValue(string valueText)
        {
            if (string.IsNullOrWhiteSpace(valueText))
                return null;

            try
            {
                var date = DateTime.ParseExact(valueText, "yyyy-MM-dd", CultureInfo.InvariantCulture.DateTimeFormat);
                return TenantTime.FromDate(date);
            }
            catch (FormatException)
            {
                throw new Exception("Invalid date: '" + valueText + "'");
            }
        }
    }

    class EnumChecker : IValueChecker
    {
        private Type enumType;

        public EnumChecker(Type enumType)
        {
            this.enumType = enumType;
        }

        public bool AreEqual(object actualValue, object expectedValue) => actualValue.Equals(expectedValue);
        public object ParseValue(string valueText) => Enum.Parse(enumType, valueText);
    }
}
