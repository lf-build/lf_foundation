﻿using LendFoundry.Foundation.Date;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;

namespace LendFoundry.Foundation.Testing
{
    public class ListParser
    {
        private const string IgnoreColumn = "#";
        private ITenantTime TenantTime;

        public ListParser() : this(new UtcTenantTime())
        {
        }

        public ListParser(ITenantTime tenantTime)
        {
            TenantTime = tenantTime;
        }

        public static IEnumerable<T> Parse<T>(string table)
        {
            return new ListParser().ParseList<T>(table);
        }

        public static IEnumerable<T> Parse<T>(string table, ITenantTime tenantTime)
        {
            return new ListParser(tenantTime).ParseList<T>(table);
        }

        private IEnumerable<T> ParseList<T>(string table)
        {
            var expectedTable = TableParser.Parse(table);
            var propertyNames = expectedTable.Header.Values;
            var list = new List<T>();

            foreach(var row in expectedTable.Body)
                list.Add(Parse<T>(row, propertyNames));

            return list;
        }

        private T Parse<T>(TableRow row, string[] propertyNames)
        {
            var obj = (T)Activator.CreateInstance(typeof(T));

            for (var columnIndex = 0; columnIndex < propertyNames.Length; columnIndex++)
            {
                var propertyName = propertyNames[columnIndex];
                var propertyValue = row.Values[columnIndex];

                if (propertyName.StartsWith(IgnoreColumn))
                    continue;

                if (propertyName.Contains("."))
                {
                    if(!string.IsNullOrWhiteSpace(propertyValue))
                    {
                        var names = propertyName.Split('.');
                        var parentPropertyName = names[0];
                        var parentProperty = GetProperty(obj, parentPropertyName);
                        var parentPropertyValue = parentProperty.GetValue(obj);

                        if (parentPropertyValue == null)
                        {
                            parentPropertyValue = Activator.CreateInstance(parentProperty.PropertyType);
                            parentProperty.SetValue(obj, parentPropertyValue);
                        }

                        var childPropertyName = names[1];
                        SetProperty(parentPropertyValue, childPropertyName, propertyValue);
                    }
                }
                else
                {
                    SetProperty(obj, propertyName, propertyValue);
                }
            }

            return obj;
        }

        private void SetProperty<T>(T obj, string propertyName, string stringValue)
        {
            var property = GetProperty(obj, propertyName);
            var propertyType = property.PropertyType;
            var value = Parse(stringValue, propertyType);
            property.SetValue(obj, value);
        }

        private PropertyInfo GetProperty(object obj, string propertyName)
        {
            var property = obj.GetType().GetProperty(propertyName);

            if (property == null)
                throw new Exception("Property not found: " + propertyName);

            return property;
        }

        private object Parse(string stringValue, Type propertyType)
        {
            if (propertyType == typeof(string)) return stringValue;
            if (propertyType == typeof(double)) return string.IsNullOrWhiteSpace(stringValue) ? 0 : double.Parse(stringValue, CultureInfo.InvariantCulture.NumberFormat);
            if (propertyType == typeof(int)) return string.IsNullOrWhiteSpace(stringValue) ? 0 : int.Parse(stringValue);
            if (propertyType == typeof(int?)) return string.IsNullOrWhiteSpace(stringValue) ? (object)null : int.Parse(stringValue);
            if (propertyType.IsEnum) return Enum.Parse(propertyType, stringValue);

            if (propertyType == typeof(DateTime) || propertyType == typeof(DateTime?))
                if (string.IsNullOrWhiteSpace(stringValue))
                    return null;
                else
                    return DateTime.ParseExact(stringValue, "yyyy-MM-dd", CultureInfo.InvariantCulture);

            if (propertyType == typeof(DateTimeOffset) || propertyType == typeof(DateTimeOffset?))
                if (string.IsNullOrWhiteSpace(stringValue))
                    return null;
                else
                {
                    var date = DateTime.ParseExact(stringValue, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                    return TenantTime.FromDate(date);
                }

            if (propertyType.IsClass)
            {
                if(!propertyType.GetConstructors().Any(c => c.GetParameters().Length == 0))
                    throw new Exception("Type does not have an empty constructor: " + propertyType);

                if (string.IsNullOrWhiteSpace(stringValue))
                    return null;
                else
                    return Activator.CreateInstance(propertyType);
            }

            throw new Exception("Unhandled property type: " + propertyType);
        }
    }
}
