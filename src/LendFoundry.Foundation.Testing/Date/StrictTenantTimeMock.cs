﻿using System;
using LendFoundry.Foundation.Date;

namespace LendFoundry.Foundation.Testing.Date
{
    /// <summary>
    /// A tenant time mock that requires the value of Today to be set.
    /// </summary>
    /// <remarks>
    /// An exception is thrown on attempt to get today's date without setting it to a date first.
    /// </remarks>
    public class StrictTenantTimeMock : ITenantTime
    {
        private DateTimeOffset? today;

        public StrictTenantTimeMock() : this(new UtcTenantTime())
        {
        }

        public StrictTenantTimeMock(ITenantTime fallback)
        {
            Fallback = fallback;
        }

        private ITenantTime Fallback { get; }

        public TimeZoneInfo TimeZone => Fallback.TimeZone;
        public DateTimeOffset Convert(DateTimeOffset dateTime) => Fallback.Convert(dateTime);
        public DateTimeOffset Create(int year, int month, int day) => Fallback.Create(year, month, day);
        public DateTimeOffset FromDate(DateTime dateTime) => Fallback.FromDate(dateTime);

        public DateTimeOffset Today
        {
            get
            {
                if (today == null)
                    throw new Exception("The value of Today is not set");

                return today.Value;
            }
            set
            {
                today = value;
            }
        }

        public DateTimeOffset Now
        {
            get
            {
                if (today == null)
                    throw new Exception("The value of Now is not set");

                return Today;
            }
        }
    }
}
