﻿using LendFoundry.Foundation.Date;
using System;

namespace LendFoundry.Foundation.Testing.Date
{
    /// <summary>
    /// A tenant time that allows Today to be set to any arbitrary date.
    /// </summary>
    /// <remarks>
    /// If Today is not set or is set to null, it gets today's date from the fallback tenant time.
    /// </remarks>
    public class TenantTimeMock : ITenantTime
    {
        private DateTimeOffset? today;

        public TenantTimeMock() : this(new UtcTenantTime())
        {
        }

        public TenantTimeMock(ITenantTime fallback)
        {
            Fallback = fallback;
        }

        private ITenantTime Fallback { get; }

        public DateTimeOffset Today
        {
            get { return today ?? Fallback.Today; }
            set { today = value; }
        }

        public DateTimeOffset Now => today ?? Fallback.Now;
        public TimeZoneInfo TimeZone => Fallback.TimeZone;
        public DateTimeOffset Convert(DateTimeOffset dateTime) => Fallback.Convert(dateTime);
        public DateTimeOffset Create(int year, int month, int day) => Fallback.Create(year, month, day);
        public DateTimeOffset FromDate(DateTime dateTime) => Fallback.FromDate(dateTime);
    }
}
