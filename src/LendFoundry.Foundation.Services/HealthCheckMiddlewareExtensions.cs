﻿#if DOTNET2
using Microsoft.AspNetCore.Builder;
#else
using Microsoft.AspNet.Builder;
#endif

namespace LendFoundry.Foundation.Services
{
    public static class HealthCheckMiddlewareExtensions
    {
        public static IApplicationBuilder UseHealthCheck(this IApplicationBuilder app)
        {
            return app.UseMiddleware<HealthCheckMiddleware>();
        }
    }
}
