﻿namespace LendFoundry.Foundation.Services
{
    public class Constants
    {
        public const string Merchant = "merchant";

        public const string Application = "application";

        public const string Loan = "loan";

        public const string Borrower = "borrower";
    }
}