﻿namespace LendFoundry.Foundation.Services.Handlebars
{
    public interface IHandlebarsResolver
    {
        void Start();
    }
}