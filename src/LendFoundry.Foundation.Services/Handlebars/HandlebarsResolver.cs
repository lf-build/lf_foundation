﻿namespace LendFoundry.Foundation.Services.Handlebars
{
    public class HandlebarsResolver : IHandlebarsResolver
    {
        public void Start()
        {
            Register();
        }

        private static void Register()
        {
            HandlebarsDotNet.Handlebars.RegisterHelper("ifc", (writer, options, context, arguments) =>
            {
                // TODO: Implement compared by type(int, string...)
                string arg1 = (string)arguments[0];
                string arg2 = (string)arguments[1];

                if (arg1.Equals(arg2))
                {
                    options.Template(writer, (object)context);
                }
                else
                {
                    options.Inverse(writer, (object)context);
                }
            });
        }
    }
}