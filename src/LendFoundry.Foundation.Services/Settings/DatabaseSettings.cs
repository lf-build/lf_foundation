using System;

namespace LendFoundry.Foundation.Services.Settings
{
    [Obsolete("will be removed in next release")]
    public class DatabaseSettings
    {
        public DatabaseSettings(string connectionStringVariable, string defaultConnectionString, string databaseVariable, string defaultDatabase)
        {
            ConnectionString = Environment.GetEnvironmentVariable(connectionStringVariable) ?? defaultConnectionString;
            Database = Environment.GetEnvironmentVariable(databaseVariable) ?? defaultDatabase;
        }

        public string ConnectionString { get; set; }

        public string Database { get; set; }
    }
}