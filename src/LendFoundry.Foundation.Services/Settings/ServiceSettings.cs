using System;

namespace LendFoundry.Foundation.Services.Settings
{
    [Obsolete("will be removed in next release")]
    public class ServiceSettings
    {
        public ServiceSettings(string service, string defaultHost, int defaultPort = 5000)
            : this($"{service.ToUpper()}_HOST", defaultHost, $"{service.ToUpper()}_PORT", defaultPort)
        {
        }

        public ServiceSettings(string hostVariable, string defaultHost, string portVariable, int defaultPort = 5000)
        {
            Host = Environment.GetEnvironmentVariable(hostVariable) ?? defaultHost;
            var port = Environment.GetEnvironmentVariable(portVariable);
            Port = !string.IsNullOrEmpty(port) ? int.Parse(port) : defaultPort;
        }

        public string Host { get; set; }

        public int Port { get; set; }
    }
}