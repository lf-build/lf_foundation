using System.Collections.Generic;
using LendFoundry.Foundation.Client;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif
namespace LendFoundry.Foundation.Services
{
    public class ErrorResult : ObjectResult
    {
        public ErrorResult(int code, string message) : base(new Error(code, message))
        {
            StatusCode = code;
        }

        public ErrorResult(int code, IEnumerable<Error> errors) : base(errors)
        {
            StatusCode = code;
        }

        public ErrorResult(int code, string message, string field) : base(new FieldError(code, message, field))
        {
            StatusCode = code;
        }

        public static ErrorResult BadRequest(string message)
        {
            return new ErrorResult(400, message);
        }

        public static ErrorResult BadRequest(IEnumerable<Error> errors)
        {
            return new ErrorResult(400, errors);
        }

        public static ErrorResult BadRequest(string message, string parameterName)
        {
            return new ErrorResult(400, message, parameterName);
        }

        public static ErrorResult NotFound(string message)
        {
            return new ErrorResult(404, message);
        }

        public static ErrorResult NotFound(IEnumerable<Error> errors)
        {
            return new ErrorResult(404, errors);
        }

        public static ErrorResult InternalServerError(string message)
        {
            return new ErrorResult(500, message);
        }

        public static ErrorResult InternalServerError(IEnumerable<Error> errors)
        {
            return new ErrorResult(500, errors);
        }
    }
}