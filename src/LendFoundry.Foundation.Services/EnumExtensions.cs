﻿using System;

namespace LendFoundry.Foundation.Services
{
    public class EnumExtensions
    {
        public static string GetEnumListStringNames<T>()
        {
            return string.Join<string>(", ", Enum.GetNames(typeof(T)));
        }

        public static string[] GetEnumListNames<T>()
        {
            return Enum.GetNames(typeof(T));
        }

        public static string GetEnumNames<T>()
        {
            return $"Supported values => { GetEnumListStringNames<T>().Replace("Undefined,", string.Empty).Trim() }";
        }
    }
}