﻿using Jil;
using LendFoundry.Security.Tokens;
using System;
using System.Threading.Tasks;
using LendFoundry.Foundation.Client;
#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
#else
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Http;
#endif

namespace LendFoundry.Foundation.Services
{
    public class ErrorHandlingMiddleware
    {
        public ErrorHandlingMiddleware(RequestDelegate next)
        {
            Next = next;
        }

        private RequestDelegate Next { get; }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await Next.Invoke(context);
            }
            catch (InvalidTokenException exception)
            {
                WriteError(StatusCodes.Status401Unauthorized, exception.Message, context.Response);
            }
            catch (NotFoundException)
            {
                var response = context.Response;

                if (response.HasStarted)
                    return;

                response.StatusCode = StatusCodes.Status404NotFound;
            }
            catch (InvalidArgumentException invalidArgument)
            {
                WriteError(StatusCodes.Status400BadRequest, invalidArgument.Message, context.Response);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                WriteError(StatusCodes.Status500InternalServerError, $"We couldn't process your request, please contact customer support with this information : {e.Message}", context.Response);
            }
        }

        private async void WriteError(int statusCode, string errorMessage, HttpResponse response)
        {
            if (response.HasStarted)
                return;

            response.StatusCode = statusCode;
            response.ContentType = "application/json";
            await response.WriteAsync(JSON.Serialize(new Error(statusCode, errorMessage)));
        }
    }
}