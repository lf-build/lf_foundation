﻿#if DOTNET2
using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
#else
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
#endif
namespace LendFoundry.Foundation.Services
{
    public static class CorsExtensions
    {
        public static IApplicationBuilder UseCors(this IApplicationBuilder app, IHostingEnvironment env)
        {
            return app.UseCors(builder =>
            {
                if (env.IsDevelopment())
                    builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader().SetPreflightMaxAge(TimeSpan.FromHours(24));
                else
                {
                    builder
                        .AllowAnyMethod()
                        .WithHeaders("Authorization")
                        .WithOrigins("*").SetPreflightMaxAge(TimeSpan.FromHours(24));
                }
            });
        }
    }
}