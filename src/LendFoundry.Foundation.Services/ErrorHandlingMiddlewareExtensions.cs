﻿#if DOTNET2
using Microsoft.AspNetCore.Builder;
#else
using Microsoft.AspNet.Builder;
#endif
namespace LendFoundry.Foundation.Services
{
    public static class ErrorHandlingMiddlewareExtensions
    {
        public static IApplicationBuilder UseErrorHandling(this IApplicationBuilder app)
        {
            return app.UseMiddleware<ErrorHandlingMiddleware>();
        }
    }
}