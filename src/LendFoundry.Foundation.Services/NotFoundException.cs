using System;

namespace LendFoundry.Foundation.Services
{
    public class NotFoundException : Exception
    {
        public NotFoundException(string message) : base(message)
        {

        }
    }
}