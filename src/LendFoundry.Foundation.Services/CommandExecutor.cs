﻿using LendFoundry.Foundation.Logging;
using System;
using System.Collections.Generic;

namespace LendFoundry.Foundation.Services
{
    public class Command
    {
        public Command(Action execute, Action rollback)
        {
            DoExecute = execute;
            DoRollback = rollback;
        }

        private bool Executed { get; set; }
        private Action DoExecute { get; }
        private Action DoRollback { get; }

        public void Execute()
        {
            DoExecute();
            Executed = true;
        }

        public void Rollback()
        {
            if (Executed)
                DoRollback();
        }
    }

    public class CommandExecutor
    {
        private ILogger Logger { get; }

        public CommandExecutor(ILogger logger)
        {
            Logger = logger;
        }

        public void Execute(List<Command> commands)
        {
            try
            {
                foreach (var command in commands)
                    command.Execute();
            }
            catch (Exception exception)
            {
                Logger.Error("Failed to execute a step", exception);
                Rollback(commands);
                throw;
            }
        }

        private void Rollback(IEnumerable<Command> commands)
        {
            foreach (var command in commands)
            {
                try
                {
                    command.Rollback();
                }
                catch (Exception exception)
                {
                    Logger.Error("Failed to rollback a step", exception);
                    // We don't rethrow the exception because all commands must be rolledback
                }
            }
        }
    }
}