﻿namespace LendFoundry.Foundation.Services
{
    public class FieldError
    {
        public FieldError()
        {
        }

        public FieldError(string message)
        {
            Message = message;
        }

        public FieldError(int code, string message) : this(message)
        {
            Code = code;
        }

        public FieldError(int code, string message, string field) : this(code, message)
        {
            Field = field;
        }

        public int Code { get; set; }

        public string Message { get; set; }

        public string Field { get; set; }
    }
}