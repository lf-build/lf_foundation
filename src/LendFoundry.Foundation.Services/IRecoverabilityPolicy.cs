﻿using System;

namespace LendFoundry.Foundation.Services
{
    public interface IRecoverabilityPolicy
    {
        bool CanBeRecoveredFrom(Exception exception);
    }
}
