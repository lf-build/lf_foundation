﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using System;
using System.Threading.Tasks;
using LendFoundry.Foundation.Client;
#if DOTNET2
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
#endif
namespace LendFoundry.Foundation.Services
{
    public abstract class ExtendedController : Controller
    {
        protected ExtendedController()
        {
        }

        protected ExtendedController(ILogger logger)
        {
            Logger = logger;
        }

        protected ILogger Logger { get; }

        protected IActionResult Execute(Func<IActionResult> expression)
        {
            try
            {
                return expression();
            }
            catch (InvalidTokenException ex)
            {
                return new ErrorResult(StatusCodes.Status401Unauthorized, ex.Message);
            }
            catch (InvalidArgumentException ex)
            {
                if (!string.IsNullOrWhiteSpace(ex.ParamName))
                {
                    return ErrorResult.BadRequest(ex.Message, ex.ParamName);
                }
                return ErrorResult.BadRequest(ex.Message);
            }
            catch (InvalidOperationException ex)
            {
                return ErrorResult.BadRequest(ex.Message);
            }
            catch (ArgumentNullException ex)
            {
                return ErrorResult.BadRequest(ex.Message);
            }
            catch (ArgumentException ex)
            {
                return ErrorResult.BadRequest(ex.Message);
            }
            catch (NotFoundException ex)
            {
                return ErrorResult.NotFound(ex.Message);
            }
            catch (ClientException ex)
            {
                Logger?.Error($"{nameof(ClientException)} raised", ex);
                return new ErrorResult(ex.Error.Code, ex.Error.Message);
            }
        }

        protected async Task<IActionResult> ExecuteAsync(Func<Task<IActionResult>> expression)
        {
            try
            {
                return await expression();
            }
            catch (InvalidTokenException ex)
            {
                return new ErrorResult(StatusCodes.Status401Unauthorized, ex.Message);
            }
            catch (InvalidArgumentException ex)
            {
                if (!string.IsNullOrWhiteSpace(ex.ParamName))
                {
                    return ErrorResult.BadRequest(ex.Message, ex.ParamName);
                }
                return ErrorResult.BadRequest(ex.Message);
            }
            catch (InvalidOperationException ex)
            {
                return ErrorResult.BadRequest(ex.Message);
            }
            catch (ArgumentNullException ex)
            {
                return ErrorResult.BadRequest(ex.Message);
            }
            catch (ArgumentException ex)
            {
                return ErrorResult.BadRequest(ex.Message);
            }
            catch (NotFoundException ex)
            {
                return ErrorResult.NotFound(ex.Message);
            }
            catch (ClientException ex)
            {
                Logger?.Error($"{nameof(ClientException)} raised", ex);
                return new ErrorResult(ex.Error.Code, ex.Error.Message);
            }
        }
    }
}