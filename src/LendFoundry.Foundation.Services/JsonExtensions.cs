﻿#if DOTNET2
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.AspNet.Mvc;
using Microsoft.Framework.DependencyInjection;
#endif
using Newtonsoft.Json.Serialization;
using System;
using LendFoundry.Foundation.Client;

namespace LendFoundry.Foundation.Services
{
    public static class JsonExtensions
    {
        public static IMvcBuilder AddLendFoundryJsonOptions(this IMvcBuilder builder)
        {
            return AddLendFoundryJsonOptions(builder, (options) => { });
        }

        public static IMvcBuilder AddLendFoundryJsonOptions(this IMvcBuilder builder, Action<MvcJsonOptions> addExtraOptions)
        {
            builder.AddJsonOptions(options =>
            {
                addExtraOptions(options);

                options.SerializerSettings.Converters.Add(new CustomEnumConverter());
                options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            });
            return builder;
        }

        public static MvcJsonOptions AddInterfaceConverter<TInterface, TConcrete>(this MvcJsonOptions jsonOptions) where TConcrete : class, TInterface
        {
            jsonOptions.SerializerSettings.Converters.Add(new InterfaceConverter<TInterface, TConcrete>());
            return jsonOptions;
        }
    }
}