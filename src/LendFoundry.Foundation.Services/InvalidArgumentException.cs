﻿using System;

namespace LendFoundry.Foundation.Services
{
    public class InvalidArgumentException : ArgumentException
    {
        public new string Message { get; set; }

        public new string ParamName { get; set; }

        public InvalidArgumentException(string message) : base(message)
        {
            Message = message;
        }

        public InvalidArgumentException(string message, Exception innerException) : base(message, innerException)
        {
            Message = message;
        }

        public InvalidArgumentException(string message, string paramName) : base(message, paramName)
        {
            Message = message;
            ParamName = paramName;
        }

        public InvalidArgumentException(string message, string paramName, Exception innerException) : base(message, paramName, innerException)
        {
            Message = message;
            ParamName = paramName;
        }
    }
}