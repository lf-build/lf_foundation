﻿using Newtonsoft.Json.Linq;
using System;
using System.Dynamic;
using System.Linq;
using System.Text.RegularExpressions;

namespace LendFoundry.Foundation.Services
{
    public static class InterpolateExtentions
    {
        public static Regex defaultPropertyRegex = new Regex(@"\{([A-Za-z0-9_]+(\.[A-Za-z0-9_]+)*)\}");

        public static object GetPropertyValue(string propertyName, object obj)
        {
            //NOTE: the reflaction is not working with Jobject, so added below code.
            var jObject = obj as JObject;
            if (jObject != null)
                return jObject.SelectToken(propertyName).Value<object>();

            var dotIndex = propertyName.IndexOf('.');
            var isNestedProperty = dotIndex > -1;

            return isNestedProperty
                ? GetNestedPropertyValue(propertyName, obj, 0, dotIndex)
                : obj.GetType().GetProperty(propertyName)?.GetValue(obj);
        }

        public static object GetNestedPropertyValue(string nestedPropertyName, object obj, int startIndex, int endIndex)
        {
            var isLastPropertyName = endIndex == -1;
            var propertyNameLength = (isLastPropertyName ? nestedPropertyName.Length : endIndex) - startIndex;
            var propertyName = nestedPropertyName.Substring(startIndex, propertyNameLength);
            var propertyValue = obj.GetType().GetProperty(propertyName)?.GetValue(obj);

            // Try get a property when it cames from a JSON object.
            var property = obj as JObject;
            if (property != null)
                propertyValue = property.GetValue(propertyName);

            if (propertyValue == null)
                return null;

            if (isLastPropertyName)
                return propertyValue;

            var nextEndIndex = nestedPropertyName.IndexOf('.', endIndex + 1);
            return GetNestedPropertyValue(nestedPropertyName, propertyValue, endIndex + 1, nextEndIndex);
        }

        public static string FormatWith(this string message, object properties, Regex regex = null)
        {
            if (string.IsNullOrWhiteSpace(message))
                return null;

            if (regex == null)
                regex = defaultPropertyRegex;

            if (IsHandlebars(message))
            {
                message = ResolveHandlebars(message, properties).ToString();
            }

            var match = regex.Match(message);

            if (!match.Success)
                return message;

            var result = message;

            do
            {
                var propertyName = match.Groups[1].Value;
                var propertyValue = GetPropertyValue(propertyName, properties);

                if (propertyValue == null)
                    throw new ArgumentException($"The Property name: #{propertyName} was not found!");

                if (IsHandlebars(propertyValue))
                {
                    propertyValue = ResolveHandlebars(propertyValue, properties);
                }

                var placeholder = match.Value;
                result = result.Replace(placeholder, propertyValue?.ToString() ?? "null");
                match = match.NextMatch();
            }
            while (match.Success);

            return result;
        }

        private static object ResolveHandlebars(object value, object properties)
        {
            try
            {
                var eventinfo = JObject.FromObject(properties).ToObject<dynamic>();

                var json = Newtonsoft.Json.JsonConvert.DeserializeObject<ExpandoObject>(eventinfo.Data.ToString());

                var template = HandlebarsDotNet.Handlebars.Compile(value.ToString());

                var result = template(json);

                return result;
            }
            catch (Exception ex)
            {
                throw new ArgumentException("Logical Expression Invalid", ex.Message);
            }
        }

        private static bool IsHandlebars(object value)
        {
            if(value == null)
                return false;

            var message = value.ToString();
            if (message.IndexOf("{{") >= 0)
                return true;
            return value != null && HandlebarsDotNet.Handlebars.Configuration.BlockHelpers.Keys.Any(key => message.Contains(key));
        }
    }
}