﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;

namespace LendFoundry.Foundation.Services
{
    public class CustomEnumConverter : StringEnumConverter
    {
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            try
            {
                return base.ReadJson(reader, objectType, existingValue, serializer);
            }
            catch (Exception)
            {
                //gets Undefined as default enum value to avoid break serialization
                return existingValue;
            }
        }
    }
}