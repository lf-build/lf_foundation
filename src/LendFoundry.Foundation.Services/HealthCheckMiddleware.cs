﻿#if DOTNET2
using Microsoft.AspNetCore.Http;
#else
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Builder;
#endif
using System.Threading.Tasks;

namespace LendFoundry.Foundation.Services
{
    public class HealthCheckMiddleware
    {
        public HealthCheckMiddleware(RequestDelegate next)
        {
            Next = next;
        }

        private RequestDelegate Next { get; }

        public async Task Invoke(HttpContext context)
        {
            var method = context.Request.Method.ToUpper();

            if (method == "GET")
            {
                var path = context.Request.Path;

                if (path == "/healthcheck")
                {
                    context.Response.StatusCode = StatusCodes.Status204NoContent;
                    return;
                }

                if (path == "/ping")
                {
                    context.Response.StatusCode = StatusCodes.Status200OK;
                    await context.Response.WriteAsync("pong");
                    return;
                }
            }

            await Next.Invoke(context);
        }
    }
}
