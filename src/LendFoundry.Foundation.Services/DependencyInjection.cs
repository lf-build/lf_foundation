using System;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
namespace LendFoundry.Foundation.Services
{
    public abstract class DependencyInjection
    {
        protected DependencyInjection()
        {
            Provider = ConfigureServices(new ServiceCollection()).BuildServiceProvider();
        }

        protected IServiceProvider Provider { get; }

        protected abstract IServiceCollection ConfigureServices(IServiceCollection services);
    }
}