﻿using System;

namespace LendFoundry.Foundation.Services
{
    public class AlwaysRetryPolicy : IRecoverabilityPolicy
    {
        public bool CanBeRecoveredFrom(Exception ex)
        {
            return true;
        }
    }

}
