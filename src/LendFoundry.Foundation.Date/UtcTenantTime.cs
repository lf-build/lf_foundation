﻿using System;

namespace LendFoundry.Foundation.Date
{
    public class UtcTenantTime : ITenantTime
    {
        public DateTimeOffset Now => DateTimeOffset.UtcNow;

        public DateTimeOffset Today => Create(Now.Year, Now.Month, Now.Day);

        public TimeZoneInfo TimeZone => TimeZoneInfo.Utc;

        public DateTimeOffset Create(int year, int month, int day) => new DateTimeOffset(year, month, day, 0, 0, 0, TimeSpan.Zero);

        public DateTimeOffset Convert(DateTimeOffset dateTime) => TimeZoneInfo.ConvertTime(dateTime, TimeZone);

        public DateTimeOffset FromDate(DateTime dateTime) => Create(dateTime.Year, dateTime.Month, dateTime.Day);
    }
}
