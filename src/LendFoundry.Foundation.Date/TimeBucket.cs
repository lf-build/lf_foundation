﻿using System;
using System.Globalization;

namespace LendFoundry.Foundation.Date
{
    public class TimeBucket
    {
        private DateTimeOffset time;

        public TimeBucket()
            : this(DateTimeOffset.Now)
        {
        }

        public TimeBucket(DateTimeOffset time)
        {
            Time = time;
        }

        public DateTimeOffset Time
        {
            get { return time; }
            set
            {
                time = value;
                Hour = string.Format("{0:yyyy-MM-dd-HH}-h", value);
                Day = string.Format("{0:yyyy-MM-dd}-d", value);
                Week = string.Format("{0:yyyy}-{1:00}-w", value, GetWeekOfYear(value));
                Month = string.Format("{0:yyyy-MM}-m", value);
                Quarter = string.Format("{0:yyyy}-{1}-q", value, (value.Month - 1) / 3 + 1);
                Year = string.Format("{0:yyyy}-y", value);                
            }
        }

        public string Hour { get; private set; }

        public string Day { get; private set; }

        public string Week { get; private set; }

        public string Month { get; private set; }

        public string Quarter { get; private set; }

        public string Year { get; private set; }

        private static int GetWeekOfYear(DateTimeOffset date)
        {
            var culture = CultureInfo.CurrentCulture;
            var format = culture.DateTimeFormat;
            return culture.Calendar.GetWeekOfYear(date.DateTime, format.CalendarWeekRule, format.FirstDayOfWeek);
        }
    }
}
