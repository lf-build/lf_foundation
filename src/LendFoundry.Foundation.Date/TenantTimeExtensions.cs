using LendFoundry.Configuration;
using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
namespace LendFoundry.Foundation.Date
{
    public static class TenantTimeExtensions
    {
        public static IServiceCollection AddTenantTime(this IServiceCollection services)
        {
            services.AddTransient<ITenantTimeFactory, TenantTimeFactory>();
            services.AddTransient(p => p.GetService<ITenantTimeFactory>().Create(p.GetService<IConfigurationServiceFactory>(), p.GetService<ITokenReader>()));
            return services;
        }
    }
}
