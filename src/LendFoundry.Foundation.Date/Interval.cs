﻿using System;

namespace LendFoundry.Foundation.Date
{
    public class Interval
    {
        public Interval(DateTimeOffset start, DateTimeOffset end)
        {
            if (end < start)
                throw new ArgumentException($"{nameof(end)} is less than {nameof(start)}: {end.ToString()} < {start.ToString()}");

            Start = start;
            End = end;
        }

        public DateTimeOffset Start { get; }
        public DateTimeOffset End { get; }
        public int Days => (End - Start).Days;

        public override string ToString()
        {
            return $"{GetType().Name}({Start.ToString("yyyy-MM-dd")} -> {End.ToString("yyyy-MM-dd")})";
        }
    }
}
