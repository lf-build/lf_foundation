using System;

namespace LendFoundry.Foundation.Date
{
    public static class TimeExtensions
    {
        public static TimeSpan To(this DateTimeOffset first, DateTimeOffset second)
        {
            return first - second;
        }

        public static bool IsShorterThan(this TimeSpan timeSpan, TimeSpan amount)
        {
            return timeSpan > amount;
        }

        public static bool IsLongerThan(this TimeSpan timeSpan, TimeSpan amount)
        {
            return timeSpan < amount;
        }

        public static bool IsBusinessDay(this DateTimeOffset date) =>
                      date.DayOfWeek != DayOfWeek.Saturday &&
                      date.DayOfWeek != DayOfWeek.Sunday;

        public static int BusinessDaysTo(this DateTimeOffset fromDate, DateTimeOffset toDate)
        { 
            var ret = 0;
            var dt = fromDate;
            while (dt < toDate)
            {
                if (dt.IsBusinessDay()) ret++;                
                dt = dt.AddDays(1);
            }
            return ret;
        }

        public static int CalendarDaysTo(this DateTimeOffset fromDate, DateTimeOffset toDate)
        { 
            var ret = 0; 
            var dt = fromDate;
            while (dt < toDate) { ret++; dt = dt.AddDays(1); }
            return ret;
        }
    }
}