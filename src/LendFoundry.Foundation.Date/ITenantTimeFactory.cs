using LendFoundry.Configuration;
using LendFoundry.Security.Tokens;

namespace LendFoundry.Foundation.Date
{
    public interface ITenantTimeFactory
    {
        ITenantTime Create(IConfigurationServiceFactory configurationFactory, ITokenReader tokenReader);
    }
}