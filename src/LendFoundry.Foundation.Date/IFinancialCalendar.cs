﻿using System;

namespace LendFoundry.Foundation.Date
{
    public interface IFinancialCalendar
    {
        int DaysInMonth(int month);
        int DaysBetween(DateTimeOffset startDate, DateTimeOffset endDate);
    }
}
