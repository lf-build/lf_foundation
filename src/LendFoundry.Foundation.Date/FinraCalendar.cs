﻿using System;

namespace LendFoundry.Foundation.Date
{
    /// <summary>
    ///    The FINRA (Financial Industry Regulatory Authority is an investor protection non-governmental agency. Per the FINRA
    ///    Interest shall be computed on the basis of a 360-day year, i.e., every calendar month shall be considered to be 1/12 of 360 days; 
    ///    every period from a date in one month to the same date in the following month shall be considered to be 30 days.
    ///    Note: The number of elapsed days should be computed in accordance with the examples given in the following table:
    ///
    ///        From 1st to 30th of the same month to be figured as 29 days;
    ///        From 1st to 31st of the same month to be figured as 30 days;
    ///        From 1st to 1st of the following month to be figured as 30 days;
    ///        From 1st to 28th of February to be figured as 27 days;
    ///        From the 23rd of February to the 3rd of March is to be figured as 10 days;
    ///        From the 15th of May to the 6th of June is to be figured as 21 days.
    ///        Where interest is payable on 30th or 31st of the month:
    ///
    ///        From 30th or 31st to 1st of the following month to be figured as 1 day;
    ///        From 30th or 31st to 30th of the following month to be figured as 30 days;
    ///        From 30th or 31st to 31st of the following month to be figured as 30 days;
    ///        From 30th or 31st to 1st of second following month to be figured as 1 month, 1 day.
    ///
    ///     U.S. (NASD) method. 
    ///     "If the starting date is the last day of a month, it becomes equal to the 30th day of 
    ///     the same month. If the ending date is the last day of a month and the starting date is 
    ///     earlier than the 30th day of a month, the ending date becomes equal to the 1st day of the next month; 
    ///     otherwise the ending date becomes equal to the 30th day of the same month."
    /// </summary>
    public class FinraCalendar : IFinancialCalendar
    {
        public static readonly IFinancialCalendar Instance = new FinraCalendar();

        public int DaysInMonth(int month)
        {
            return 30;
        }

        public int DaysBetween(DateTimeOffset startDate, DateTimeOffset endDate)
        {
            return DaysBetween(startDate, endDate, true);
        }

        public int DaysBetween(DateTimeOffset startDate, DateTimeOffset endDate, bool isNASD)
        {
            int startDay, endDay;
            int daysBetween;

            if (endDate < startDate) return 0;

            startDay = startDate.Day;
            endDay = endDate.Day;

            CheckDays(startDate, endDate, ref startDay, ref endDay, isNASD);

            if (IsSameMonth(startDate, endDate))
            {
                daysBetween = endDay - startDay;
                if (daysBetween < 0) daysBetween = 0;

                return daysBetween;
            }
            daysBetween = (30 - startDay) + DaysInFullMonth(startDate, endDate) + endDay;

            if (daysBetween < 0) daysBetween = 0;

            return daysBetween;
        }

        private int DaysInFullMonth(DateTimeOffset startDate, DateTimeOffset endDate)
        {
            int fullMonthDays = (endDate.Month - startDate.Month - 1) * 30 + (endDate.Year - startDate.Year) * 360;
            if (fullMonthDays < 0) fullMonthDays = 0;

            return fullMonthDays;
        }

        private static bool IsSameMonth(DateTimeOffset startDate, DateTimeOffset endDate)
        {
            return (startDate.Month == endDate.Month && startDate.Year == endDate.Year);
        }

        private static void CheckDays(DateTimeOffset startDate, DateTimeOffset endDate, ref int startDay, ref int endDay, bool isNASD)
        {
            if (isNASD)
            {
                if (LastDayOfMonth(startDate) && startDate.Month == 2)
                    startDay = 30;

                if (endDay == 31 && startDay >= 30)
                    endDay = 30;

                if (startDay == 31)
                    startDay = 30;
            }
            else
            {
                if (startDay == 31) startDay = 30;
                if (endDay == 31) endDay = 30;
            }
        }

        private static bool LastDayOfMonth(DateTimeOffset date)
        {
            return (date.AddDays(1).Month == date.Month + 1);
        }
    }
}
