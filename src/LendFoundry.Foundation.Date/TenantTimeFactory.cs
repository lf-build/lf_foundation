using LendFoundry.Configuration;
using LendFoundry.Security.Tokens;

namespace LendFoundry.Foundation.Date
{
    public class TenantTimeFactory : ITenantTimeFactory
    {
        public ITenantTime Create(IConfigurationServiceFactory configurationFactory, ITokenReader tokenReader)
        {
            return new TenantTime(configurationFactory, tokenReader);
        }
    }
}