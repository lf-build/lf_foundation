using System;

namespace LendFoundry.Foundation.Date
{
    public interface ITenantTime
    {
        DateTimeOffset Now { get; }
        DateTimeOffset Today { get; }
        TimeZoneInfo TimeZone { get; }
        DateTimeOffset Create(int year, int month, int day);
        DateTimeOffset FromDate(DateTime dateTime);
        DateTimeOffset Convert(DateTimeOffset dateTime);
    }
}
