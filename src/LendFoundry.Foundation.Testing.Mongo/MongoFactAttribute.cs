﻿using Xunit;

namespace LendFoundry.Foundation.Testing.Mongo
{
    public class MongoFactAttribute : FactAttribute
    {
        public MongoFactAttribute()
        {
            if (!MongoTest.IsMongoDbAvailable)
                Skip = "Requires mongodb";
        }
    }
}
