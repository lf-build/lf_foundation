﻿using LendFoundry.Foundation.Persistence.Mongo;
using MongoDB.Driver;
using System;

namespace LendFoundry.Foundation.Testing.Mongo
{
    public class MongoTest
    {
        private static MongoClient mongoClient;
        private static readonly object clientLock = new object();

        private static string ConnectionString => Environment.GetEnvironmentVariable("TEST_MONGO_CONNECTION_STRING");

        public static bool IsMongoDbAvailable => !string.IsNullOrWhiteSpace(ConnectionString);

        public static void Run(Action<IMongoConfiguration> test)
        {
            AssertMongoIsAvailable();

            var configuration = CreateConfiguration();

            try
            {
                test(configuration);
            }
            finally
            {
                DropDatabase(configuration.Database);
            }
        }

        private static IMongoConfiguration CreateConfiguration()
        {
            AssertMongoIsAvailable();

            return new MongoConfiguration
            (
                database: Guid.NewGuid().ToString(),
                connectionString: ConnectionString
            );
        }

        private static MongoClient Client
        {
            get
            {
                AssertMongoIsAvailable();

                lock (clientLock)
                {
                    if (mongoClient == null)
                        mongoClient = new MongoClient(ConnectionString);
                }

                return mongoClient;
            }
        }

        private static void DropDatabase(string dbName)
        {
            AssertMongoIsAvailable();

            Client
                .DropDatabaseAsync(dbName)
                .Wait();
        }

        private static void AssertMongoIsAvailable()
        {
            if (!IsMongoDbAvailable)
                throw new Exception("Mongo is not available");
        }
    }
}
