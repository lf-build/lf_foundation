﻿namespace LendFoundry.Foundation.Logging
{
    /// <summary>
    /// RequestLoggingMiddlewareOptions
    /// </summary>
    public class RequestLoggingMiddlewareOptions
    {
        /// <summary>
        /// Excludes properties from logging
        /// </summary>
        public string[] ExcludeProperties { get; }

        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="excludeProperties"></param>
        public RequestLoggingMiddlewareOptions(string[] excludeProperties)
        {
            ExcludeProperties = excludeProperties;
        }

    }
}
