﻿namespace LendFoundry.Foundation.Logging
{
    public interface ILogFormatter
    {
        string Format(LogEntry logEntry);
    }
}
