﻿using System;

namespace LendFoundry.Foundation.Logging
{
    internal class ConsoleLogWriter : ILogWriter
    {
        public void WriteLine(string text)
        {
            Console.WriteLine(text);
        }
    }
}