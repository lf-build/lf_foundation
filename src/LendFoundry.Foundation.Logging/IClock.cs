﻿using System;

namespace LendFoundry.Foundation.Logging
{
    public interface IClock
    {
        DateTime Now();
    }
}
