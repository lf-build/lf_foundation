﻿#if DOTNET2
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.AspNet.Hosting;
using Microsoft.Framework.DependencyInjection;
#endif

namespace LendFoundry.Foundation.Logging
{
    public static class ServiceLoggingInjectionExtensions
    {
        public static IServiceCollection AddHttpServiceLogging(this IServiceCollection services, string serviceName)
        {
            services.AddSingleton<ILogWriter, ConsoleLogWriter>();
            services.AddSingleton<IClock, SystemClock>();
            services.AddSingleton<ILogContext, ServiceLogContext>();
            services.AddSingleton<ILoggerFactory>(provider =>
            {
                var env = provider.GetService<IHostingEnvironment>();
                var logContext = provider.GetService<ILogContext>();
                var logWriter = provider.GetService<ILogWriter>();
                var clock = provider.GetService<IClock>();
                return new ServiceLoggerFactory(serviceName, logContext, logWriter, clock, env);
            });
            services.AddSingleton(provider => provider.GetService<ILoggerFactory>().CreateLogger());
            return services;
        }

        public static IServiceCollection AddServiceLogging(this IServiceCollection services, string serviceName, ILogContext logContext)
        {

        #if DOTNET2
            services.AddSingleton(logContext);
        #else
            services.AddInstance(logContext);
        #endif

            services.AddSingleton<ILogWriter, ConsoleLogWriter>();
            services.AddSingleton<IClock, SystemClock>();
            services.AddSingleton<ILoggerFactory>(provider =>
            {
                var logWriter = provider.GetService<ILogWriter>();
                var clock = provider.GetService<IClock>();
                return new ServiceLoggerFactory(serviceName, logContext, logWriter, clock, null);
            });
            services.AddSingleton(provider => provider.GetService<ILoggerFactory>().CreateLogger());
            return services;
        }
    }
}
