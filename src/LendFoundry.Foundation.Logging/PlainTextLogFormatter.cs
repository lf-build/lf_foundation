﻿using System;
using Jil;

namespace LendFoundry.Foundation.Logging
{
    public class PlainTextLogFormatter : ILogFormatter
    {
        public string Format(LogEntry logEntry)
        {
            var text = $"{logEntry.Timestamp} {logEntry.Service} [{logEntry.Level}] {logEntry.Message}";
            text += GetHttpInfo(logEntry);
            text += GetExceptionInfo(logEntry);
            return text;
        }

        private static string GetExceptionInfo(LogEntry logEntry)
        {
            if (string.IsNullOrWhiteSpace(logEntry.Exception))
                return "";

            return $"\n{logEntry.Exception}";
        }

        private static string GetHttpInfo(LogEntry logEntry)
        {
            var request = GetPropertyValue("request", logEntry.Properties);
            var response = GetPropertyValue("response", logEntry.Properties);
            return
                //Let's leave out the http headers for now to reduce verbosity
                //NewLine("headers", GetPropertyValue("headers", request)) +
                NewLine("body", GetPropertyValue("body", request)) +
                NewLine("response", response);
        }

        private static object GetPropertyValue(string name, object obj)
        {
            var property = obj?.GetType().GetProperty(name);
            return property?.GetValue(obj);
        }

        private static string NewLine(string name, object data)
        {
            if (data == null)
                return "";

            return $"\n{name}: { SerializeAsJson(data) }";
        }

        private static string SerializeAsJson(object data)
        {
            return JSON.SerializeDynamic(data, Options.ISO8601ExcludeNullsUtcCamelCase);
        }
    }
}
