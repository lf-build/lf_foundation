﻿namespace LendFoundry.Foundation.Logging
{
    public class LogEntry
    {
        public object Context { get; internal set; }
        public string Exception { get; internal set; }
        public string Level { get; internal set; }
        public string Message { get; internal set; }
        public object Properties { get; internal set; }
        public string Service { get; internal set; }
        public string Timestamp { get; internal set; }
        public object ExceptionDetail { get; internal set; }
        public string LoggingDurationInMillSeconds {get;internal set;}
    }
}
