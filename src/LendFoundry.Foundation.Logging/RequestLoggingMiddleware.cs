﻿#if DOTNET2
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
#else
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Http.Internal;
#endif

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using LendFoundry.Security.Tokens;

namespace LendFoundry.Foundation.Logging
{
    public class RequestLoggingMiddleware
    {
        private RequestDelegate Next { get; }
        private ILogger Logger { get; }
        private RequestLoggingMiddlewareOptions Options { get; }

        public RequestLoggingMiddleware(RequestDelegate next, ILogger logger, RequestLoggingMiddlewareOptions options)
        {
            Next = next;
            Logger = logger;
            Options = options;
        }

        public async Task Invoke(HttpContext context)
        {
            // The MVC middleware reads the body stream before we do, which
            // means that the body stream's cursor would be positioned at the end.
            // We need to reset the body stream cursor position to zero in order
            // to read it. As the default body stream implementation does not
            // support seeking, we need to explicitly enable rewinding of the stream:
            context.Request.EnableRewind();


            var startedAt = DateTime.Now;

            try
            {
                await Next.Invoke(context);
            }
            catch (InvalidTokenException){
                //No need to log invalid token exception
                throw;
            }
            catch (Exception exception)
            {
                Logger.Error("Error while processing request", exception, new
                {
                    request = GetRequestData(context.Request),
                }, false);

                throw;
            }

            var finishedAt = DateTime.Now;
            var request = context.Request;
            var response = context.Response;

            Logger.Info("{request.method} {request.path}", new
            {
                request = GetRequestData(request),
                response = GetResponseData(response),
                startedAt = startedAt,
                finishedAt = finishedAt
            }, false);
        }

        private object GetRequestData(HttpRequest request)
        {
            object body = ReadBody(request);
            return new
            {
                method = request.Method,
                path = request.Path.Value,
                queryString = request.QueryString.ToString(),
                body = new { contents = $"{body}" },
                headers = request.Headers.Copy()
            };
        }

        private object GetResponseData(HttpResponse response)
        {
            return new
            {
                status = response.StatusCode,
                headers = response.Headers.Copy()

                // The actual response body stream does not support
                // rewinding. For more details, see: https://github.com/aspnet/Home/issues/487#issuecomment-97695667
                //body = ReadBody(response)
            };
        }

        private object ReadBody(HttpRequest request)
        {
            return ReadBody(request.Body, request.ContentType);
        }

        private object ReadBody(HttpResponse response)
        {
            return ReadBody(response.Body, response.ContentType);
        }

        private object ReadBody(Stream bodyStream, string contentType)
        {
            if (contentType != null)
            {
                // Must use StartsWith() because the content type may
                // include enconding information, for example: application/json; charset=utf-8
                if (contentType.StartsWith("application/json"))
                    return ReadAsJson(bodyStream);

                else if (contentType.StartsWith("text/"))
                    return ReadAsString(bodyStream);
            }

            return null;

        }

        private bool Contains(string content, string[] values)
        {
            if (values == null || !values.Any())
                return false;

            foreach (var item in values)
            {
                if (content.IndexOf(item,StringComparison.InvariantCultureIgnoreCase) >= 0)
                    return true;
            }

            return false;
        }

        private object ReadAsJson(Stream bodyStream)
        {
            var contents = ReadAsString(bodyStream);
            if (contents == null)
                return null;

            if (Contains(contents, Options.ExcludeProperties))
            {
                try
                {
                    JObject jObject = JObject.Parse(contents);
                    var allFieldsToBeRemoved = FindAllFieldsToBeRemoved(jObject, Options.ExcludeProperties).ToList();
                    foreach (var item in allFieldsToBeRemoved)
                    {
                        var item1 = jObject.SelectToken(item.Path);
                        item1.Replace("XXXX");
                    }
                    return jObject;
                }
                catch (Exception ex)
                {
                    Logger.Error("Error occured while replacing the value of ExcludeProperties", ex);
                    dynamic bodyObject = Jil.JSON.DeserializeDynamic(contents);
                    return bodyObject;
                }
            }
            else
            {
                dynamic bodyObject = Jil.JSON.DeserializeDynamic(contents);
                return bodyObject;
            }

        }
        private IEnumerable<JToken> FindAllFieldsToBeRemoved(JObject obj, string[] fields)
        {
            var toSearch = new Stack<JToken>(obj.Children());
            while (toSearch.Count > 0)
            {
                var inspected = toSearch.Pop();
                if (inspected.Type == JTokenType.Property && fields.Contains(((JProperty)inspected).Name, StringComparer.InvariantCultureIgnoreCase))
                {
                    yield return inspected;
                }
                foreach (var child in inspected)
                {
                    toSearch.Push(child);
                }
            }
        }

        private string ReadAsString(Stream stream)
        {
            // Because the body was read by the MVC middleware,
            // we have to reset the stream position:
            stream.Position = 0;

            // We must set leaveOpen=true to prevent the StreamReader from closing
            // the underlying stream:
            var reader = new StreamReader(stream, Encoding.UTF8, true, 1024, true);

            if (reader.EndOfStream)
                return null;

            var contents = reader.ReadToEnd();

            // We must reset the stream's position, otherwise it would not
            // be readable anyfrom from this point on:
            stream.Position = 0;

            return contents;
        }
    }
}
