﻿#if DOTNET2
using Microsoft.AspNetCore.Hosting;
#else
using Microsoft.AspNet.Hosting;
#endif
using System;

namespace LendFoundry.Foundation.Logging
{
    public class ServiceLoggerFactory : ILoggerFactory
    {
        public ServiceLoggerFactory(string serviceName, ILogContext logContext, ILogWriter logWriter, IClock clock, IHostingEnvironment env)
        {
            ServiceName = serviceName;
            LogContext = logContext;
            LogWriter = logWriter;
            Clock = clock;
            LogFormatter = CreateLogFormatter(env);
        }

        private string ServiceName { get; }

        private ILogContext LogContext { get; }

        private ILogFormatter LogFormatter { get; }

        private ILogWriter LogWriter { get; }

        private IClock Clock { get; }

        private static string LogFormat => Environment.GetEnvironmentVariable("LOG_FORMAT");
        
        public ILogger CreateLogger()
        {
            return new ServiceLogger(ServiceName, LogWriter, LogContext, Clock, LogFormatter);
        }

        public ILogger Create(ILogContext context)
        {
            return new ServiceLogger(ServiceName, LogWriter, context, Clock, LogFormatter);
        }

        private static ILogFormatter CreateLogFormatter(IHostingEnvironment env)
        {
            if (LogFormat?.ToLower() == "plain")
                return new PlainTextLogFormatter();
            return new JsonLogFormatter(false);
        }
    }
}
