﻿namespace LendFoundry.Foundation.Logging
{
    public interface ILoggerFactory
    {
        ILogger CreateLogger();

        ILogger Create(ILogContext context);
    }
}
