﻿using System;

namespace LendFoundry.Foundation.Logging
{
    internal class SystemClock : IClock
    {
        public DateTime Now()
        {
            return DateTime.Now;
        }
    }
}