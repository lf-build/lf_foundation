﻿namespace LendFoundry.Foundation.Logging
{
    public interface ILogContext
    {
        object Data { get; }
    }
}
