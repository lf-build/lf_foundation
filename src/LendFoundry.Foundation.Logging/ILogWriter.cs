﻿namespace LendFoundry.Foundation.Logging
{
    public interface ILogWriter
    {
        void WriteLine(string text);
    }
}