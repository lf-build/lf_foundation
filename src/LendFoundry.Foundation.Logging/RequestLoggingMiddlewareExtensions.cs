﻿#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
#else
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Http;
#endif
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json.Linq;
using System;

namespace LendFoundry.Foundation.Logging
{
    public static class RequestLoggingMiddlewareExtensions
    {
        public static IApplicationBuilder UseRequestLogging(this IApplicationBuilder app)
        {
            return app.UseMiddleware<RequestLoggingMiddleware>(new RequestLoggingMiddlewareOptions(null));
        }

        public static IApplicationBuilder UseRequestLogging(this IApplicationBuilder app, RequestLoggingMiddlewareOptions options)
        {
            return app.UseMiddleware<RequestLoggingMiddleware>(options);
        }

        internal static Dictionary<string, object> Copy(this IHeaderDictionary headers)
        {
            var copy = new Dictionary<string, object>();

            foreach (var pair in headers)
            {
                for (int i = 0; i < pair.Value.Count(); i++)
                {
                    var headerValue = pair.Value[i];
                    var key = pair.Key + (i == 0 ? "" : i.ToString());
                    if (pair.Key.Equals("Authorization", StringComparison.InvariantCultureIgnoreCase))
                    {    
                        try
                        {
                            copy.Add(key, DecodeToken(headerValue));
                        }
                        catch (Exception)
                        {
                            copy.Add(key, new { headerValue });
                        }
                    }
                    else
                    {
                        copy.Add(key, headerValue);
                    }
                }
            }
            return copy;
        }

        internal static JObject DecodeToken(string headerValue)
        {
            if(!string.IsNullOrWhiteSpace(headerValue) && headerValue.Contains("Bearer"))
            {
                headerValue = headerValue.Replace("Bearer", "").Trim();
                var splittedToken = headerValue.Split('.');
                var payload = splittedToken[1];
                var s = Convert.FromBase64String(payload.PadRight(payload.Length + (4 - payload.Length % 4) % 4, '='));
                var data = Encoding.UTF8.GetString(s);
                return JObject.Parse(data);
            }
            return null;
        }
    }
}
