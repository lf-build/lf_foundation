﻿using System;

namespace LendFoundry.Foundation.Logging
{
    public interface ILogger
    {
        void Info(string message);
        void Info(string message, object properties, bool propertiesAsJson=true);
        void Debug(string message);
        void Debug(string message, object properties, bool propertiesAsJson = true);
        void Warn(string message);
        void Warn(string message, object properties, bool propertiesAsJson = true);
        void Error(string message);
        void Error(string message, object properties, bool propertiesAsJson = true);
        void Error(string message, Exception exception);
        void Error(string message, Exception exception, object properties, bool propertiesAsJson = true);
        bool IsEnable(LogLevel logLevel);
    }
}
