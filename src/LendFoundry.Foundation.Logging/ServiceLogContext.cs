﻿#if DOTNET2
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.PlatformAbstractions;
#else
using Microsoft.AspNet.Http;
#endif
using System.Linq;

namespace LendFoundry.Foundation.Logging
{
    public class ServiceLogContext : ILogContext
    {
        public const string XRequestIdHeader = "X-Request-Id";
        public const string Authorization = "Authorization";

        private readonly IHttpContextAccessor httpContextAccessor;

        public ServiceLogContext(IHttpContextAccessor httpContextAccessor)
        {
            this.httpContextAccessor = httpContextAccessor;
        }

        public object Data
        {
            get
            {
                return new
                {
                    requestId = httpContextAccessor?.HttpContext?.Request?.Headers[XRequestIdHeader].FirstOrDefault(),
                    tokenDetails = RequestLoggingMiddlewareExtensions.DecodeToken(httpContextAccessor?.HttpContext?.Request?.Headers[Authorization].FirstOrDefault()),
                    serviceVersion = PlatformServices.Default.Application.ApplicationVersion
                };
            }
        }
    }
}
