﻿namespace LendFoundry.Foundation.Logging
{
    public class NullLogContext : ILogContext
    {
        public static readonly ILogContext Instance = new NullLogContext();

        public object Data
        {
            get
            {
                return null;
            }
        }
    }
}
