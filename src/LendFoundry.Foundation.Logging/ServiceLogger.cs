﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace LendFoundry.Foundation.Logging
{
    internal class ServiceLogger : ILogger
    {
        private static readonly Regex PropertyPlaceholderRegex = new Regex(@"\{([A-Za-z0-9_]+(\.[A-Za-z0-9_]+)*)\}");

        private string serviceName;
        private ILogWriter writer;
        private ILogContext logContext;
        private IClock clock;
        private ILogFormatter formatter;
        private LogLevel logLevel = LogLevel.Error;
        private bool LogLoggingTime = false;

        public ServiceLogger(string serviceName, ILogWriter writer, ILogContext logContext, IClock clock, ILogFormatter formatter)
        {
            var logLevelRaw = Environment.GetEnvironmentVariable("LOG_LEVEL");
            if (!string.IsNullOrWhiteSpace(logLevelRaw))
            {
                if (!(Enum.TryParse<LogLevel>(logLevelRaw, true, out logLevel)))
                {
                    logLevel = LogLevel.Error;
                }
            }
            
            var logLoggingTime = Environment.GetEnvironmentVariable("LOG_LOGGINGTIME");
            if (!string.IsNullOrWhiteSpace(logLoggingTime))
            {
                LogLoggingTime = true;
            }

            this.logContext = logContext;
            this.serviceName = serviceName;
            this.writer = writer;
            this.clock = clock;
            this.formatter = formatter;
        }

        public void Debug(string message)
        {
            Log(LogLevel.Debug, message, null, null);
        }

        public void Debug(string message, object properties, bool propertiesAsJson = true)
        {
            Log(LogLevel.Debug, message, null, properties, propertiesAsJson);
        }

        public void Info(string message)
        {
            Log(LogLevel.Info, message, null, null);
        }

        public void Info(string message, object properties, bool propertiesAsJson = true)
        {
            Log(LogLevel.Info, message, null, properties, propertiesAsJson);
        }

        public void Warn(string message)
        {
            Log(LogLevel.Warning, message, null, null);
        }

        public void Warn(string message, object properties, bool propertiesAsJson = true)
        {
            Log(LogLevel.Warning, message, null, properties, propertiesAsJson);
        }

        public void Error(string message)
        {
            Log(LogLevel.Error, message, null, null);
        }

        public void Error(string message, object properties, bool propertiesAsJson = true)
        {
            Log(LogLevel.Error, message, null, properties, propertiesAsJson);
        }

        public void Error(string message, Exception exception)
        {
            Log(LogLevel.Error, message, exception, null);
        }

        public void Error(string message, Exception exception, object properties, bool propertiesAsJson = true)
        {
            Log(LogLevel.Error, message, exception, properties, propertiesAsJson);
        }

        private void Log(LogLevel level, string message, Exception exception, object properties, bool propertiesAsJson = true)
        {

            if ((int)logLevel > (int)level)
            {
                return;
            }
            var watch = System.Diagnostics.Stopwatch.StartNew();

            try
            {
                var finalMessage = properties == null ? message : Interpolate(message, properties);
                JObject exceptionDetail=null;
                if (exception != null)
                {
                    try
                    {
                        var t = JsonConvert.SerializeObject(exception);
                        exceptionDetail = JObject.Parse(t);
                    }
                    catch (Exception)
                    {
                    }
                }

                object propertiesData = properties;
                if (propertiesAsJson == true && properties!=null)
                {
                    propertiesData = new { Value = $"[properties ]: {JObject.FromObject(properties)}" };
                }


                var logEntry = new LogEntry
                {
                    Service = serviceName,
                    Level = level.ToString(),
                    Message = finalMessage,
                    Timestamp = clock.Now().ToString("o"),
                    Properties = propertiesData,
                    Exception = exception?.ToString(),
                    Context = logContext?.Data,
                    ExceptionDetail = exceptionDetail,
                    LoggingDurationInMillSeconds = "##LoggingDurationInMillSeconds##"
                };
                var content=formatter.Format(logEntry);
                watch.Stop();
                var elapsedMs = watch.ElapsedMilliseconds;
                if (LogLoggingTime)
                {
                    watch.Start();
                }
                content = content.Replace("\"##LoggingDurationInMillSeconds##\"", elapsedMs.ToString());
                writer.WriteLine(content);

                if (LogLoggingTime)
                {
                    watch.Stop();
                    elapsedMs = watch.ElapsedMilliseconds;
                    Console.WriteLine("Total Logging Time:" + elapsedMs.ToString());
                }
            }
            catch (Exception)
            {
                watch.Stop();
                throw;
            }

        }

        private string Interpolate(string message, object properties)
        {
            Match match = PropertyPlaceholderRegex.Match(message);

            if (!match.Success)
                return message;

            string result = message;

            do
            {
                string propertyName = match.Groups[1].Value;
                object propertyValue = GetPropertyValue(propertyName, properties);
                string placeholder = match.Value;
                result = result.Replace(placeholder, propertyValue == null ? "null" : propertyValue.ToString());
                match = match.NextMatch();
            }
            while (match.Success);

            return result;
        }

        private static object GetPropertyValue(string propertyName, object obj)
        {
            var dotIndex = propertyName.IndexOf('.');
            var isNestedProperty = dotIndex > -1;

            if (isNestedProperty)
                return GetNestedPropertyValue(propertyName, obj, 0, dotIndex);
            else
                return obj.GetType().GetProperty(propertyName)?.GetValue(obj);
        }

        private static object GetNestedPropertyValue(string nestedPropertyName, object obj, int startIndex, int endIndex)
        {
            bool isLastPropertyName = endIndex == -1;
            int propertyNameLength = (isLastPropertyName ? nestedPropertyName.Length : endIndex) - startIndex;
            string propertyName = nestedPropertyName.Substring(startIndex, propertyNameLength);
            object propertyValue = obj.GetType().GetProperty(propertyName)?.GetValue(obj);

            if (propertyValue == null)
                return null;

            if (isLastPropertyName)
                return propertyValue;

            int nextEndIndex = nestedPropertyName.IndexOf('.', endIndex + 1);
            return GetNestedPropertyValue(nestedPropertyName, propertyValue, endIndex + 1, nextEndIndex);
        }

        public bool IsEnable(LogLevel level)
        {
            if ((int)logLevel > (int)level)
            {
                return false;
            }
            return true;
        }
    }
}
