﻿namespace LendFoundry.Foundation.Logging
{
    public class JsonLogFormatter : ILogFormatter
    {
        public JsonLogFormatter(bool prettyPrint)
        {
            Options = prettyPrint ?
                Jil.Options.ISO8601PrettyPrintExcludeNullsUtcCamelCase :
                Jil.Options.ISO8601ExcludeNullsUtcCamelCase;
        }

        private Jil.Options Options { get; }

        public string Format(LogEntry logEntry)
        {
            return Jil.JSON.SerializeDynamic(logEntry, Options);
        }
    }
}
