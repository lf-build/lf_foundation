﻿using System;
using MongoDB.Bson.Serialization;
using LendFoundry.Security.Encryption;

namespace LendFoundry.Foundation.Persistence.Mongo
{
    public class BsonEncryptor<TInterface, TConcrete> : IBsonSerializer<TInterface> where TConcrete : TInterface
    {
        private IEncryptionService EncrypterService { get; set; }

        public BsonEncryptor(IEncryptionService encrypterService)
        {
            EncrypterService = encrypterService;
        }

        public TInterface Deserialize(BsonDeserializationContext context, BsonDeserializationArgs args)
        {
            var jsonString = context.Reader.ReadString();
            return EncrypterService.Decrypt<TConcrete>(jsonString);
        }

        public void Serialize(BsonSerializationContext context, BsonSerializationArgs args, TInterface value)
        {
            var valueAsJson = EncrypterService.Encrypt(value);
            context.Writer.WriteString(valueAsJson);
        }

        public Type ValueType => typeof(TInterface);

        public void Serialize(BsonSerializationContext context, BsonSerializationArgs args, object value)
        {
            var valueAsJson = EncrypterService.Encrypt(value);
            context.Writer.WriteString(valueAsJson);
        }

        object IBsonSerializer.Deserialize(BsonDeserializationContext context, BsonDeserializationArgs args)
        {
            var jsonString = context.Reader.ReadString();
            return EncrypterService.Decrypt<TConcrete>(jsonString);
        }
    }
}