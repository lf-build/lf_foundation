﻿using System;
using MongoDB.Bson.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace LendFoundry.Foundation.Persistence.Mongo
{
    public class BsonJsonSerializer<T> : IBsonSerializer<T>
    {
        private JsonSerializerSettings Settings { get; }= new JsonSerializerSettings()
        {
            ContractResolver = new CamelCasePropertyNamesContractResolver()
        };

        public T Deserialize(BsonDeserializationContext context, BsonDeserializationArgs args)
        {

            var jsonString = context.Reader.ReadString();
            return JsonConvert.DeserializeObject<T>(jsonString, Settings);
        }

        public void Serialize(BsonSerializationContext context, BsonSerializationArgs args, T value)
        {
            var valueAsJson = JsonConvert.SerializeObject(value, Settings);
            context.Writer.WriteString(valueAsJson);
        }

        public Type ValueType => typeof(T);

        public void Serialize(BsonSerializationContext context, BsonSerializationArgs args, object value)
        {
            var valueAsJson = JsonConvert.SerializeObject(value, Settings);
            context.Writer.WriteString(valueAsJson);
        }

        object IBsonSerializer.Deserialize(BsonDeserializationContext context, BsonDeserializationArgs args)
        {
            var jsonString = context.Reader.ReadString();
            return JsonConvert.DeserializeObject<T>(jsonString, Settings);
        }
    }
}