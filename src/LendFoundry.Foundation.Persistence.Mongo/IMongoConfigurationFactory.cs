﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
#if DOTNET2
#else
using Microsoft.Framework.DependencyInjection;
#endif
namespace LendFoundry.Foundation.Persistence.Mongo
{
    public interface IMongoConfigurationFactory
    {
        IMongoConfiguration Get(ITokenReader reader, ILogger logger);
    }
}
