﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
namespace LendFoundry.Foundation.Persistence.Mongo
{
    public static class MongoExtensions
    {
        public static IServiceCollection AddMongoConfiguration(this IServiceCollection services, string serviceName)
        {
            services.AddTransient<IMongoConfigurationFactory>(p => new MongoConfigurationFactory(p, serviceName, null));
            services.AddTransient<IMongoConfiguration>(p => p.GetService<IMongoConfigurationFactory>().Get(p.GetRequiredService<ITokenReader>(), p.GetRequiredService<ILogger>()));
            return services;
        }

        public static IServiceCollection AddMongoConfiguration(this IServiceCollection services, string serviceName, string defaultDatabaseName )
        {
            services.AddTransient<IMongoConfigurationFactory>(p => new MongoConfigurationFactory(p, serviceName, defaultDatabaseName));
            services.AddTransient<IMongoConfiguration>(p => p.GetService<IMongoConfigurationFactory>().Get(p.GetRequiredService<ITokenReader>(), p.GetRequiredService<ILogger>()));
            return services;
        }
    }
}
