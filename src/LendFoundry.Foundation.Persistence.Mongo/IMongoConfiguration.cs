﻿namespace LendFoundry.Foundation.Persistence.Mongo
{
    public interface IMongoConfiguration
    {
        string ConnectionString { get; set; }
        string Database { get; set; }
        bool? EnableMongoLogging { get; set; }
        bool? EnableMongoConnectionPooling { get; set; }
        int? MongoMinConnectionPoolSize { get; set; }
        int? MongoMaxConnectionIdleTimeInMinutes { get; set; }
        int? MaxConnectionLifeTimeInMinutes {get;set;}
    }
}