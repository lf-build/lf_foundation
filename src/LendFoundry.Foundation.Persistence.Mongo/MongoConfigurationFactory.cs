﻿using System;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using LendFoundry.Configuration;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
namespace LendFoundry.Foundation.Persistence.Mongo
{
    internal class MongoConfigurationFactory: IMongoConfigurationFactory
    {
        private IServiceProvider Provider { get; }
        private string ServiceName { get; }
        private string DefaultDatabaseName { get; }

        public MongoConfigurationFactory(IServiceProvider provider, string serviceName, string defaultDatabaseName)
        {
            Provider = provider;
            ServiceName = serviceName;
            DefaultDatabaseName = defaultDatabaseName;
        }


        public IMongoConfiguration Get(ITokenReader reader, ILogger logger)
        {
            var configurationFactory = Provider.GetService<IConfigurationServiceFactory>();
            var defaultConfiguration = configurationFactory.Create<MongoConfiguration>("tenant", reader).Get();
            var serviceconfiguration = configurationFactory.Create<MongoConfiguration>(ServiceName, reader).Get();

            MongoConfiguration mongoConfiguration = new MongoConfiguration();
            if (!string.IsNullOrWhiteSpace(DefaultDatabaseName))
                mongoConfiguration.Database = DefaultDatabaseName;

            if (defaultConfiguration != null)
            {
                if (!string.IsNullOrWhiteSpace(defaultConfiguration.ConnectionString))
                {
                    mongoConfiguration.ConnectionString = defaultConfiguration.ConnectionString;
                }
                if (defaultConfiguration.EnableMongoLogging != null)
                {
                    mongoConfiguration.EnableMongoLogging = defaultConfiguration.EnableMongoLogging;
                }
                if (defaultConfiguration.EnableMongoConnectionPooling != null)
                {
                    mongoConfiguration.EnableMongoConnectionPooling = defaultConfiguration.EnableMongoConnectionPooling;
                }
                
                if (defaultConfiguration.MongoMaxConnectionIdleTimeInMinutes != null)
                {
                    mongoConfiguration.MongoMaxConnectionIdleTimeInMinutes = defaultConfiguration.MongoMaxConnectionIdleTimeInMinutes;
                }
                if (defaultConfiguration.MongoMinConnectionPoolSize != null)
                {
                    mongoConfiguration.MongoMinConnectionPoolSize = defaultConfiguration.MongoMinConnectionPoolSize;
                }
                if (defaultConfiguration.MaxConnectionLifeTimeInMinutes != null)
                {
                    mongoConfiguration.MaxConnectionLifeTimeInMinutes = defaultConfiguration.MaxConnectionLifeTimeInMinutes;
                }
            }


            if (serviceconfiguration != null)
            {
                if ( !string.IsNullOrWhiteSpace(serviceconfiguration.ConnectionString))
                {
                    mongoConfiguration.ConnectionString = serviceconfiguration.ConnectionString;
                }
                if (!string.IsNullOrWhiteSpace(serviceconfiguration.Database))
                {
                    mongoConfiguration.Database = serviceconfiguration.Database;
                }
                if (serviceconfiguration.EnableMongoLogging != null)
                {
                    mongoConfiguration.EnableMongoLogging = serviceconfiguration.EnableMongoLogging;
                }
                if (serviceconfiguration.EnableMongoConnectionPooling != null)
                {
                    mongoConfiguration.EnableMongoConnectionPooling = serviceconfiguration.EnableMongoConnectionPooling;
                }
                if (serviceconfiguration.MongoMaxConnectionIdleTimeInMinutes != null)
                {
                    mongoConfiguration.MongoMaxConnectionIdleTimeInMinutes = serviceconfiguration.MongoMaxConnectionIdleTimeInMinutes;
                }
                if (serviceconfiguration.MongoMinConnectionPoolSize != null)
                {
                    mongoConfiguration.MongoMinConnectionPoolSize = serviceconfiguration.MongoMinConnectionPoolSize;
                }
                if (serviceconfiguration.MaxConnectionLifeTimeInMinutes != null)
                {
                    mongoConfiguration.MaxConnectionLifeTimeInMinutes = serviceconfiguration.MaxConnectionLifeTimeInMinutes;
                }
            }
            
            if (string.IsNullOrWhiteSpace(mongoConfiguration.ConnectionString))
                throw new Exception($"Connection string not found in tenant or {ServiceName} configuration");

            if (string.IsNullOrWhiteSpace(mongoConfiguration.Database))
                throw new Exception($"database property not found in  {ServiceName} configuration");

            return mongoConfiguration;
        }

    }
}
