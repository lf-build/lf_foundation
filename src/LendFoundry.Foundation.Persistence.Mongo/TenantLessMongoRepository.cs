﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace LendFoundry.Foundation.Persistence.Mongo
{
    public abstract class TenantLessMongoRepository<TInterface, TConcrete> : IRepository<TInterface> where TInterface : IAggregate where TConcrete : Aggregate, TInterface
    {
        static TenantLessMongoRepository()
        {
            BsonSerializer.RegisterSerializer(new ImpliedImplementationInterfaceSerializer<TInterface, TConcrete>());

            if (BsonClassMap.IsClassMapRegistered(typeof(Aggregate)))
                return;

            BsonClassMap.RegisterClassMap<Aggregate>(map =>
            {
                map.AutoMap();
                map.MapIdField(m => m.Id)
                    .SetIdGenerator(StringObjectIdGenerator.Instance)
                    .SetSerializer(new StringSerializer(BsonType.ObjectId))
                    .SetIgnoreIfDefault(true);
            });
        }

        protected TenantLessMongoRepository(IMongoConfiguration configuration, string collectionName)
        {
            var client = new MongoClient(configuration.ConnectionString);
            var database = client.GetDatabase(configuration.Database);

            Collection = database.GetCollection<TInterface>(collectionName);
        }

        protected async void CreateIndexIfNotExists(string indexName, IndexKeysDefinition<TInterface> index)
        {
            await Collection.Indexes.ListAsync().ContinueWith(async t =>
            {
                var indexes = await t.Result.ToListAsync();
                if (indexes.All(i => i["name"] != indexName))
                {
                    await Collection.Indexes.CreateOneAsync(index, new CreateIndexOptions
                    {
                        Name = indexName
                    });
                }
            });
        }

        protected async void CreateIndexIfNotExists(string indexName, IndexKeysDefinition<TInterface> index, bool unique = false)
        {
            await Collection.Indexes.ListAsync().ContinueWith(async t =>
            {
                var indexes = await t.Result.ToListAsync();
                if (indexes.All(i => i["name"] != indexName))
                {
                    await Collection.Indexes.CreateOneAsync(index, new CreateIndexOptions
                    {
                        Name = indexName,
                        Unique = unique
                    });
                }
            });
        }

        protected IMongoCollection<TInterface> Collection { get; }

        protected IMongoQueryable<TInterface> Query
        {
            get
            {
                return Collection.AsQueryable();
            }
        }

        public async Task<TInterface> Get(string id)
        {
            return await Query.FirstOrDefaultAsync(i => i.Id == id);
        }

        public async Task<IEnumerable<TInterface>> All(Expression<Func<TInterface, bool>> query, int? skip = null, int? quantity = null)
        {
            var cursor = Query.Where(query);

            if (skip.HasValue)
                cursor = cursor.Skip(skip.Value);

            if (quantity.HasValue)
                cursor = cursor.Take(quantity.Value);

            return await cursor.ToListAsync();
        }

        public int Count(Expression<Func<TInterface, bool>> query)
        {
            return Query.Count(query);
        }

        public void Add(TInterface item)
        {
            EnsureIsNotNull(item);
            item.Id = ObjectId.GenerateNewId().ToString();

            Collection.InsertOne(item);
        }

        public void Remove(TInterface item)
        {
            EnsureIsNotNull(item);
            Collection.DeleteOne(FindByIdPredicate(item));
        }

        public void Update(TInterface item)
        {
            EnsureIsNotNull(item);
            Collection.ReplaceOne<TInterface>(FindByIdPredicate(item), item);
        }

        private Expression<Func<TInterface, bool>> FindByIdPredicate(TInterface item)
        {
            return i => i.Id == item.Id;
        }

        private static void EnsureIsNotNull(TInterface item)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item));
        }
    }
}