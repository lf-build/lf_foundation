﻿using System;

namespace LendFoundry.Foundation.Persistence.Mongo
{
    public class MongoConfiguration : IMongoConfiguration
    {
        public MongoConfiguration()
        {

        }

        public MongoConfiguration(string connectionString, string database)
        {
            if (string.IsNullOrEmpty(connectionString))
                throw new ArgumentNullException(nameof(connectionString));

            if (string.IsNullOrEmpty(database))
                throw new ArgumentNullException(nameof(database));

            ConnectionString = connectionString;
            Database = database;
        }

        public string ConnectionString { get; set; }

        public string Database { get; set; }
        public bool? EnableMongoLogging { get; set; }
        public bool? EnableMongoConnectionPooling { get; set; }
        public int? MongoMinConnectionPoolSize { get; set; }
        public int? MongoMaxConnectionIdleTimeInMinutes { get; set; }
        public int? MaxConnectionLifeTimeInMinutes { get; set; }
    }
}