﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System.Linq;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Net.Sockets;
using MongoDB.Driver.Core.Events;

namespace LendFoundry.Foundation.Persistence.Mongo
{
    internal static class MongoClientContainer
    {
        public static readonly ConcurrentDictionary<string, IMongoDatabase> Collection = new ConcurrentDictionary<string, IMongoDatabase>();
    } 
    
    public abstract class MongoRepository<TInterface, TConcrete> : IRepository<TInterface> where TInterface : IAggregate where TConcrete : Aggregate, TInterface
    {
        private static readonly ConcurrentDictionary<string, Tuple<IMongoCollection<TInterface>,List<string>>> Collections = new ConcurrentDictionary<string, Tuple<IMongoCollection<TInterface>,List<string>>>();

        static MongoRepository()
        {
            BsonSerializer.RegisterSerializer(new ImpliedImplementationInterfaceSerializer<TInterface, TConcrete>());

            if (BsonClassMap.IsClassMapRegistered(typeof(Aggregate)))
                return;

            BsonClassMap.RegisterClassMap<Aggregate>(map =>
            {
                map.AutoMap();
                map.MapIdField(m => m.Id)
                    .SetIdGenerator(StringObjectIdGenerator.Instance)
                    .SetSerializer(new StringSerializer(BsonType.ObjectId))
                    .SetIgnoreIfDefault(true);
            });
        }

        protected MongoRepository(ITenantService tenantService, IMongoConfiguration configuration, string collectionName)
        {
            TenantService = tenantService;

            var key = configuration.ConnectionString + configuration.Database + collectionName;
            var keyForClient = configuration.ConnectionString;

            if (!MongoClientContainer.Collection.TryGetValue(keyForClient, out var database))
            {
                var watch = Stopwatch.StartNew();
                MongoClient mongoClient;
                if ((configuration.EnableMongoConnectionPooling.HasValue && configuration.EnableMongoConnectionPooling.Value==true) || (configuration.EnableMongoLogging.HasValue==true && configuration.EnableMongoLogging.Value==true))
                {
                    var mongoUrl = new MongoUrl(configuration.ConnectionString);
                    var mongoClientSettings = MongoClientSettings.FromUrl(mongoUrl);
                    if ((configuration.EnableMongoConnectionPooling.HasValue && configuration.EnableMongoConnectionPooling.Value==true))
                    {
                        mongoClientSettings.MinConnectionPoolSize = configuration.MongoMinConnectionPoolSize ?? 2;
                        mongoClientSettings.MaxConnectionPoolSize = 100;
                        mongoClientSettings.MaxConnectionIdleTime = TimeSpan.FromMinutes(configuration.MongoMaxConnectionIdleTimeInMinutes ?? 60);
                        mongoClientSettings.MaxConnectionLifeTime = TimeSpan.FromMinutes(configuration.MaxConnectionLifeTimeInMinutes ?? 60);
                        void SocketConfigurator(Socket s) => s.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.KeepAlive, true);
                        mongoClientSettings.ClusterConfigurator = cb => cb.ConfigureTcp(tcp => tcp.With(socketConfigurator: (Action<Socket>) SocketConfigurator));
                    }

                    if ((configuration.EnableMongoLogging.HasValue==true && configuration.EnableMongoLogging.Value==true))
                    {
                        mongoClientSettings.ClusterConfigurator = clusterConfigurator =>
                        {
                            clusterConfigurator.Subscribe<CommandStartedEvent>(e =>
                            {
                                if (e.CommandName == "buildInfo" || e.CommandName == "isMaster" || e.CommandName == "getLastError" || e.CommandName == "saslContinue" || e.CommandName == "saslStart" || e.CommandName=="listIndexes")
                                    return;
                                try
                                {
                                    // Log start of command
                                    Console.WriteLine(
                                        $"{{\"level\": \"Debug\", \"timestamp\":\"{DateTime.Now:o}\",\"properties\":{{ \"Name\":\"Started\",\"Command\":\"{e.CommandName}\",\"Value\": {Newtonsoft.Json.JsonConvert.ToString(e.ToJson())} }}}}");
                                }
                                catch (Exception exception)
                                {
                                    Console.Write($"Error in start " + e.CommandName + e.Command.ToJson());
                                    Console.WriteLine(exception);
                                }
                            });
                            clusterConfigurator.Subscribe<CommandSucceededEvent>(e =>
                            {
                                try
                                {
                                    if (e.CommandName == "buildInfo" || e.CommandName == "isMaster" || e.CommandName == "getLastError" || e.CommandName == "saslContinue" || e.CommandName == "saslStart" || e.CommandName=="listIndexes")
                                        return;
                                    // Log success of command 
                                    Console.WriteLine(
                                        $"{{\"level\": \"Debug\", \"timestamp\":\"{DateTime.Now:o}\",\"properties\":{{ \"Name\":\"Success\",\"Command\":\"{e.CommandName}\",\"MongoDuration\":\"{e.Duration.TotalMilliseconds}\",\"Value\": {Newtonsoft.Json.JsonConvert.ToString(e.ToJson())} }}}}");
                                }
                                catch (Exception exception)
                                {
                                    Console.Write($"Error in start " + e.CommandName + e.Reply.ToJson());
                                    Console.WriteLine(exception);
                                }
                            });
                            // clusterConfigurator.Subscribe<ConnectionOpeningEvent>(e =>
                            // {
                            //     Console.WriteLine(
                            //         $"{{\"level\": \"Debug\", \"timestamp\":\"{DateTime.Now:o}\",\"message\":\"Connection Opening\"}}");
                            // });
                            // clusterConfigurator.Subscribe<ConnectionOpenedEvent>(e =>
                            // {
                            //     Console.WriteLine(
                            //         $"{{\"level\": \"Debug\", \"timestamp\":\"{DateTime.Now:o}\",\"message\":\"Connection Opened\",\"MongoDuration\":\"{e.Duration.TotalMilliseconds}\"}}");
                            // });
                            // clusterConfigurator.Subscribe<ConnectionClosingEvent>(e =>
                            // {                                
                            //     Console.WriteLine(
                            //         $"{{\"level\": \"Debug\", \"timestamp\":\"{DateTime.Now:o}\",\"message\":\"Connection Closing\"}}");
                            // });
                            // clusterConfigurator.Subscribe<ConnectionClosedEvent>(e =>
                            // {
                            //     Console.WriteLine(
                            //         $"{{\"level\": \"Debug\", \"timestamp\":\"{DateTime.Now:o}\",\"message\":\"Connection Closed\",\"MongoDuration\":\"{e.Duration.TotalMilliseconds}\"}}");
                            // });                            
                            clusterConfigurator.Subscribe<ConnectionPoolAddedConnectionEvent>(e =>
                            {
                                Console.WriteLine(
                                    $"{{\"level\": \"Debug\", \"timestamp\":\"{DateTime.Now:o}\",\"message\":\"Connection added to pool\",\"MongoDuration\":\"{e.Duration.TotalMilliseconds}\"}}");
                            });
                            // clusterConfigurator.Subscribe<ConnectionPoolRemovedConnectionEvent>(e =>
                            // {
                            //     Console.WriteLine(
                            //         $"{{\"level\": \"Debug\", \"timestamp\":\"{DateTime.Now:o}\",\"message\":\"Connection removed from pool\",\"MongoDuration\":\"{e.Duration.TotalMilliseconds}\"}}");
                            // });
                        };

                    }

                    mongoClient = new MongoClient(mongoClientSettings);
                }
                else
                {
                    mongoClient = new MongoClient(configuration.ConnectionString);
                }
                watch.Stop();
                database = mongoClient.GetDatabase(configuration.Database);
                MongoClientContainer.Collection.TryAdd(keyForClient, database);
                Console.WriteLine($"{{\"level\": \"Debug\", \"timestamp\":\"{DateTime.Now:o}\",\"message\":\"New client created in {watch.ElapsedMilliseconds} milliseconds with pooling {configuration.EnableMongoConnectionPooling} , minimum pool size {configuration.MongoMinConnectionPoolSize ?? 5} , timeout {configuration.MongoMaxConnectionIdleTimeInMinutes ?? 60} minutes and logging {configuration.EnableMongoLogging ?? false} \"}}");
            }

            if (!Collections.TryGetValue(key, out var tmp))
            {
                var watch = Stopwatch.StartNew();
                Collection = database.GetCollection<TInterface>(collectionName);
                Indexes=Collection.Indexes.ListAsync().Result.ToList().Select(i=>i["name"].AsString).ToList();
                Collections.TryAdd(key, new Tuple<IMongoCollection<TInterface>, List<string>>(Collection,Indexes));
                CreateIndexIfNotExists("tenant", Builders<TInterface>.IndexKeys.Ascending(i => i.TenantId));
                watch.Stop();
                Console.WriteLine($"{{\"level\": \"Debug\", \"timestamp\":\"{DateTime.Now:o}\",\"message\":\"Collection {collectionName} object created in {watch.ElapsedMilliseconds} milliseconds \"}}");
            }
            else
            {
                Collection = tmp.Item1;
                Indexes = tmp.Item2;
            }
        }

        protected async void CreateIndexIfNotExists(string indexName, IndexKeysDefinition<TInterface> index)
        {
            if (Indexes.All(i => i != indexName))
            {
                await Collection.Indexes.CreateOneAsync(index, new CreateIndexOptions
                {
                    Name = indexName
                });
                Indexes.Add(indexName);
            }
        }

        protected async void CreateIndexIfNotExists(string indexName, IndexKeysDefinition<TInterface> index, bool unique = false)
        {
            if (Indexes.All(i => i != indexName))
            {
                await Collection.Indexes.CreateOneAsync(index, new CreateIndexOptions
                {
                    Name = indexName,
                    Unique = unique
                });
                Indexes.Add(indexName);
            }
        }

        protected IMongoCollection<TInterface> Collection { get; }
        private List<string> Indexes { get; }

        protected ITenantService TenantService { get; }

        protected IMongoQueryable<TInterface> Query
        {
            get
            {
                var tenantId = TenantService.Current.Id;
                return Collection.AsQueryable().Where(i => i.TenantId == tenantId);
            }
        }

        public async Task<TInterface> Get(string id)
        {
            return await Query.FirstOrDefaultAsync(i => i.Id == id);
        }

        public async Task<IEnumerable<TInterface>> All(Expression<Func<TInterface, bool>> query, int? skip = null, int? quantity = null)
        {
            var cursor = Query.Where(query);

            if (skip.HasValue)
                cursor = cursor.Skip(skip.Value);

            if (quantity.HasValue)
                cursor = cursor.Take(quantity.Value);

            return await cursor.ToListAsync();
        }

        public int Count(Expression<Func<TInterface, bool>> query)
        {
            return Query.Count(query);
        }

        public void Add(TInterface item)
        {
            EnsureIsNotNull(item);
            item.Id = ObjectId.GenerateNewId().ToString();
            item.TenantId = TenantService.Current.Id;
            Collection.InsertOne(item);
        }

        public void Remove(TInterface item)
        {
            EnsureIsNotNull(item);
            Collection.DeleteOne(FindByIdPredicate(item));
        }

        public void Update(TInterface item)
        {
            EnsureIsNotNull(item);
            Collection.ReplaceOne<TInterface>(FindByIdPredicate(item), item);
        }

        private Expression<Func<TInterface, bool>> FindByIdPredicate(TInterface item)
        {
            return i => 
                i.TenantId == TenantService.Current.Id && 
                i.TenantId == item.TenantId && 
                i.Id == item.Id;
        }

        private static void EnsureIsNotNull(TInterface item)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item));
        }
    }
}
