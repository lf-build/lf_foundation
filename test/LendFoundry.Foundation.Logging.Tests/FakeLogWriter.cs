﻿using System;

namespace LendFoundry.Foundation.Logging.Tests
{
    public class FakeLogWriter : ILogWriter
    {
        public FakeLogWriter()
        {
        }

        public string Content { get; internal set; }

        public void WriteLine(string text)
        {
            Content += text;
        }
    }
}