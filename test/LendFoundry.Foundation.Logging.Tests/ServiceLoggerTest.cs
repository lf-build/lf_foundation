﻿using System;
using Jil;
using Moq;
using Xunit;
#if DOTNET2
using Microsoft.AspNetCore.Hosting;
#else
using Microsoft.AspNet.Hosting;
#endif
namespace LendFoundry.Foundation.Logging.Tests
{
    public class ServiceLoggerTest
    {
        private readonly FakeLogWriter logWriter = new FakeLogWriter();
        private readonly Mock<IClock> mockClock = new Mock<IClock>();
        private readonly Mock<ILogContext> mockLogContext = new Mock<ILogContext>();

        private ILogger CreateLogger(string serviceName)
        {
            var factory = new ServiceLoggerFactory(serviceName, mockLogContext.Object, logWriter, mockClock.Object, Mock.Of<IHostingEnvironment>());
            return factory.CreateLogger();
        }

        private void GivenThatCurrentTimeIs(DateTime time)
        {
            mockClock.Setup(clock => clock.Now()).Returns(time);
        }

        [Fact]
        public void FactoryDoesNotReturnNull()
        {
            var logger = CreateLogger("Bar");
            Assert.NotNull(logger);
        }

        [Fact]
        public void DebugMessage()
        {
            GivenThatCurrentTimeIs(new DateTime(2017, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc));

            var logger = CreateLogger("Accounting");

            logger.Debug("This is a simple debug message");

            AssertLogOutput(new LogEntry
            {
                service = "Accounting",
                level = "Debug",
                message = "This is a simple debug message",
                timestamp = "2017-01-01T00:00:00.0000000Z"
            });
        }

        [Fact]
        public void DebugMessageWithProperties()
        {
            GivenThatCurrentTimeIs(new DateTime(2017, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc));

            var logger = CreateLogger("Accounting");

            logger.Debug("This is a simple debug message", new
            {
                color = "Green",
                animal = "Fish"
            });

            AssertLogOutput(new LogEntry
            {
                service = "Accounting",
                level = "Debug",
                message = "This is a simple debug message",
                timestamp = "2017-01-01T00:00:00.0000000Z",
                properties = new LogProperties
                {
                    color = "Green",
                    animal = "Fish"
                }
            });
        }

        [Fact]
        public void InfoMessage()
        {
            GivenThatCurrentTimeIs(new DateTime(2015, 9, 30, 17, 36, 25, 888, DateTimeKind.Utc));

            var logger = CreateLogger("Amortization");

            logger.Info("Hello world");

            AssertLogOutput(new LogEntry
            {
                service = "Amortization",
                level = "Info",
                message = "Hello world",
                timestamp = "2015-09-30T17:36:25.8880000Z"
            });
        }

        [Fact]
        public void InfoMessageWithProperties()
        {
            GivenThatCurrentTimeIs(new DateTime(2016, 12, 3, 4, 5, 6, 777, DateTimeKind.Utc));

            var logger = CreateLogger("Calendar");

            logger.Info("The quick brown fox jumps over the lazy dog", new
            {
                color = "White",
                animal = "Dog"
            });

            AssertLogOutput(new LogEntry
            {
                service = "Calendar",
                level = "Info",
                message = "The quick brown fox jumps over the lazy dog",
                timestamp = "2016-12-03T04:05:06.7770000Z",
                properties = new LogProperties
                {
                    color = "White",
                    animal = "Dog"
                }
            });
        }

        [Fact]
        public void WarningMessage()
        {
            GivenThatCurrentTimeIs(new DateTime(2015, 9, 30, 17, 36, 25, 888, DateTimeKind.Utc));

            var logger = CreateLogger("Amortization");

            logger.Warn("Beware of dog");

            AssertLogOutput(new LogEntry
            {
                service = "Amortization",
                level = "Warning",
                message = "Beware of dog",
                timestamp = "2015-09-30T17:36:25.8880000Z"
            });
        }

        [Fact]
        public void WarningMessageWithProperties()
        {
            GivenThatCurrentTimeIs(new DateTime(2015, 9, 30, 17, 36, 25, 888, DateTimeKind.Utc));

            var logger = CreateLogger("Amortization");

            logger.Warn("Beware of panda", new
            {
                color = "Red",
                animal = "Panda"
            });

            AssertLogOutput(new LogEntry
            {
                service = "Amortization",
                level = "Warning",
                message = "Beware of panda",
                timestamp = "2015-09-30T17:36:25.8880000Z",
                properties = new LogProperties
                {
                    color = "Red",
                    animal = "Panda"
                }
            });
        }

        [Fact]
        public void ErrorMessage()
        {
            GivenThatCurrentTimeIs(new DateTime(2015, 9, 30, 17, 36, 25, 888, DateTimeKind.Utc));

            var logger = CreateLogger("MoneyMovement");

            logger.Error("Not enough money");

            AssertLogOutput(new LogEntry
            {
                service = "MoneyMovement",
                level = "Error",
                message = "Not enough money",
                timestamp = "2015-09-30T17:36:25.8880000Z"
            });
        }

        [Fact]
        public void ErrorMessageWithProperties()
        {
            GivenThatCurrentTimeIs(new DateTime(2015, 9, 30, 17, 36, 25, 888, DateTimeKind.Utc));

            var logger = CreateLogger("MoneyMovement");

            logger.Error("Not enough money", new
            {
                color = "Yellow",
                animal = "Whale"
            });

            AssertLogOutput(new LogEntry
            {
                service = "MoneyMovement",
                level = "Error",
                message = "Not enough money",
                timestamp = "2015-09-30T17:36:25.8880000Z",
                properties = new LogProperties
                {
                    color = "Yellow",
                    animal = "Whale"
                }
            });
        }

        [Fact]
        public void ErrorMessageWithException()
        {
            GivenThatCurrentTimeIs(new DateTime(2015, 9, 30, 17, 36, 25, 888, DateTimeKind.Utc));

            var logger = CreateLogger("MoneyMovement");
            var exception = new ArgumentOutOfRangeException();

            logger.Error("Not enough money", exception);

            AssertLogOutput(new LogEntry
            {
                service = "MoneyMovement",
                level = "Error",
                message = "Not enough money",
                timestamp = "2015-09-30T17:36:25.8880000Z",
                exception = exception.ToString()
            });
        }

        [Fact]
        public void ErrorMessageWithExceptionAndProperties()
        {
            GivenThatCurrentTimeIs(new DateTime(2015, 9, 30, 17, 36, 25, 888, DateTimeKind.Utc));

            var logger = CreateLogger("MoneyMovement");
            var exception = new ArgumentOutOfRangeException();

            logger.Error("Not enough money", exception, new
            {
                color = "Yellow",
                animal = "Whale"
            });

            AssertLogOutput(new LogEntry
            {
                service = "MoneyMovement",
                level = "Error",
                message = "Not enough money",
                timestamp = "2015-09-30T17:36:25.8880000Z",
                exception = exception.ToString(),
                properties = new LogProperties
                {
                    color = "Yellow",
                    animal = "Whale"
                }
            });
        }

        [Fact]
        public void MessageWithSimplePropertyInterpolation()
        {
            GivenThatCurrentTimeIs(new DateTime(2014, 12, 31, 23, 59, 59, 999, DateTimeKind.Utc));

            var logger = CreateLogger("Configuration");

            logger.Info("I had this crazy dream of a {animal} wearing a {color} tuxedo on a birthday party", new
            {
                color = "Blue",
                animal = "Zebra"
            });

            AssertLogOutput(new LogEntry
            {
                service = "Configuration",
                level = "Info",
                message = "I had this crazy dream of a Zebra wearing a Blue tuxedo on a birthday party",
                timestamp = "2014-12-31T23:59:59.9990000Z",
                properties = new LogProperties
                {
                    color = "Blue",
                    animal = "Zebra"
                }
            });
        }

        [Fact]
        public void MessageWithNestedPropertyInterpolation()
        {
            GivenThatCurrentTimeIs(new DateTime(2017, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc));

            var logger = CreateLogger("EventHub");

            logger.Debug("Monday is {habits.diet.monday} day, but wednesday is {habits.diet.wednesday} day", new
            {
                habits = new
                {
                    diet = new
                    {
                        monday = "Sushi",
                        tuesday = "Thai",
                        wednesday = "Italian"
                    }
                }
            });

            AssertLogOutput(new LogEntry
            {
                service = "EventHub",
                level = "Debug",
                message = "Monday is Sushi day, but wednesday is Italian day",
                timestamp = "2017-01-01T00:00:00.0000000Z",
                properties = new LogProperties
                {
                    habits = new Habits
                    {
                        diet = new Diet
                        {
                            monday = "Sushi",
                            tuesday = "Thai",
                            wednesday = "Italian"
                        }
                    }
                }
            });
        }

        [Fact]
        public void MessageWithNestedPropertyInterpolationAndNullPropertyInBetween()
        {
            GivenThatCurrentTimeIs(new DateTime(2017, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc));

            var logger = CreateLogger("Notification");

            logger.Warn("Tuesday is {habits.diet.monday} day", new
            {
                habits = new
                {
                    diet = (object)null
                }
            });

            AssertLogOutput(new LogEntry
            {
                service = "Notification",
                level = "Warning",
                message = "Tuesday is null day",
                timestamp = "2017-01-01T00:00:00.0000000Z",
                properties = new LogProperties
                {
                    habits = new Habits
                    {
                        diet = null
                    }
                }
            });
        }

        [Fact]
        public void PropertyNamesInPascalCaseShouldBeConvertedToCamelCase()
        {
            GivenThatCurrentTimeIs(new DateTime(2015, 9, 30, 17, 36, 25, 888, DateTimeKind.Utc));

            var logger = CreateLogger("Amortization");

            logger.Info("Property names in PascalCase should be converted to {InterestingPropertyNames.PascalCase}, but interpolated variables should be in the original case", new
            {
                InterestingPropertyNames = new
                {
                    PascalCase = "camelCase"
                }
            });

            AssertLogOutputContains("\"interestingPropertyNames\"");
            AssertLogOutputContains("\"pascalCase\"");

            AssertLogOutputDoesNotContain("\"InterestingPropertyNames\"");
            AssertLogOutputDoesNotContain("\"PascalCase\"");

            AssertLogOutput(new LogEntry
            {
                service = "Amortization",
                level = "Info",
                message = "Property names in PascalCase should be converted to camelCase, but interpolated variables should be in the original case",
                timestamp = "2015-09-30T17:36:25.8880000Z",
                properties = new LogProperties
                {
                    interestingPropertyNames = new InterestingPropertyNames
                    {
                        pascalCase = "camelCase"
                    }
                }
            });
        }

        [Fact]
        public void AllowNumbersAndUnderscoresInPropertyNames()
        {
            GivenThatCurrentTimeIs(new DateTime(2015, 9, 30, 17, 36, 25, 888, DateTimeKind.Utc));

            var logger = CreateLogger("Amortization");

            logger.Info("We need {InterestingPropertyNames.With_2_Numb3rs_And_Underscores}", new
            {
                InterestingPropertyNames = new
                {
                    With_2_Numb3rs_And_Underscores = "numbers and underscores"
                }
            });

            AssertLogOutput(new LogEntry
            {
                service = "Amortization",
                level = "Info",
                message = "We need numbers and underscores",
                timestamp = "2015-09-30T17:36:25.8880000Z",
                properties = new LogProperties
                {
                    interestingPropertyNames = new InterestingPropertyNames
                    {
                        with_2_Numb3rs_And_Underscores = "numbers and underscores"
                    }
                }
            });
        }

        [Fact]
        public void DoubleQuotesAreAllowedInTheLogMessage()
        {
            GivenThatCurrentTimeIs(new DateTime(2015, 9, 30, 17, 36, 25, 888, DateTimeKind.Utc));

            var logger = CreateLogger("Amortization");

            logger.Info("Hello \"world\"");

            AssertLogOutput(new LogEntry
            {
                service = "Amortization",
                level = "Info",
                message = "Hello \"world\"",
                timestamp = "2015-09-30T17:36:25.8880000Z"
            });
        }

        [Fact]
        public void ReplaceNonexistentPropertiesWithNull()
        {
            GivenThatCurrentTimeIs(new DateTime(2015, 9, 30, 17, 36, 25, 888, DateTimeKind.Utc));

            var logger = CreateLogger("Amortization");

            logger.Info("There is {foo} here and {foo.bar.baz} there", new
            {
                color = "Green",
                animal = "Mosquito"
            });

            AssertLogOutput(new LogEntry
            {
                service = "Amortization",
                level = "Info",
                message = "There is null here and null there",
                timestamp = "2015-09-30T17:36:25.8880000Z",
                properties = new LogProperties
                {
                    color = "Green",
                    animal = "Mosquito"
                }
            });
        }

        [Fact]
        public void ContextInfoIsAddedToTheLogOutputWhenAvailable()
        {
            GivenThatCurrentTimeIs(new DateTime(2015, 9, 30, 17, 36, 25, 888, DateTimeKind.Utc));

            var logger = CreateLogger("Amortization");

            mockLogContext.Setup(context => context.Data).Returns(new
            {
                someArbitraryNumber = 97531,
                someArbitraryText = "Hello world"
            });

            logger.Info("Some context info is expected", new
            {
                color = "Green",
                animal = "Mosquito"
            });

            AssertLogOutput(new LogEntry
            {
                context = new Context
                {
                    someArbitraryNumber = 97531,
                    someArbitraryText = "Hello world"
                },
                service = "Amortization",
                level = "Info",
                message = "Some context info is expected",
                timestamp = "2015-09-30T17:36:25.8880000Z",
                properties = new LogProperties
                {
                    color = "Green",
                    animal = "Mosquito"
                }
            });
        }

        [Fact]
        public void DateTimePropertiesAreWrittenToLogUsingRoundtripFormatAndUtc()
        {
            GivenThatCurrentTimeIs(new DateTime(2015, 9, 30, 17, 36, 25, 888, DateTimeKind.Utc));

            var logger = CreateLogger("Amortization");

            var birthday = new DateTime(1982, 5, 11, 0, 0, 0, DateTimeKind.Utc);

            logger.Info("Some context info is expected", new
            {
                birthday
            });

            AssertLogOutput(new LogEntry
            {
                service = "Amortization",
                level = "Info",
                message = "Some context info is expected",
                timestamp = "2015-09-30T17:36:25.8880000Z",
                properties = new LogProperties
                {
                    birthday = birthday.ToString("yyyy-MM-ddTHH:mm:ssZ")
                }
            });
        }

        [Fact]
        public void NullJsonPropertiesAreOmitted()
        {
            var logger = CreateLogger("Amortization");

            logger.Info("No exception, nor properties");

            Assert.DoesNotContain("\"exception\"", logWriter.Content);
            Assert.DoesNotContain("\"properties\"", logWriter.Content);
        }

        private void AssertLogOutput(LogEntry expectedLogEntry)
        {
            Assert.NotNull(expectedLogEntry);

            LogEntry actualLogEntry = GetActualLogEntry();

            Assert.Equal(expectedLogEntry.service, actualLogEntry.service);
            Assert.Equal(expectedLogEntry.level, actualLogEntry.level);
            Assert.Equal(expectedLogEntry.message, actualLogEntry.message);
            Assert.Equal(expectedLogEntry.timestamp, actualLogEntry.timestamp);
            Assert.Equal(expectedLogEntry.exception, actualLogEntry.exception);

            AssertLogContext(expectedLogEntry.context, actualLogEntry.context);
            AssertLogProperties(expectedLogEntry.properties, actualLogEntry.properties);
        }

        private void AssertLogContext(Context expectedContext, Context actualContext)
        {
            if(expectedContext == null)
                Assert.Null(actualContext);
            else
            {
                Assert.NotNull(actualContext);
                Assert.Equal(expectedContext.someArbitraryNumber, actualContext.someArbitraryNumber);
                Assert.Equal(expectedContext.someArbitraryText, actualContext.someArbitraryText);
            }
        }

        private static void AssertLogProperties(LogProperties expectedProperties, LogProperties actualProperties)
        {
            if (expectedProperties == null)
                Assert.Null(actualProperties);
            else
            {
                Assert.NotNull(actualProperties);
                Assert.Equal(expectedProperties.color, actualProperties.color);
                Assert.Equal(expectedProperties.animal, actualProperties.animal);
                Assert.Equal(expectedProperties.birthday, actualProperties.birthday);

                if (expectedProperties.habits == null)
                    Assert.Null(actualProperties.habits);
                else
                {
                    if (expectedProperties.habits.diet == null)
                        Assert.Null(actualProperties.habits.diet);
                    else
                    {
                        Assert.Equal(expectedProperties.habits.diet.monday, actualProperties.habits.diet.monday);
                        Assert.Equal(expectedProperties.habits.diet.tuesday, actualProperties.habits.diet.tuesday);
                        Assert.Equal(expectedProperties.habits.diet.wednesday, actualProperties.habits.diet.wednesday);
                    }
                }
            }
        }

        private void AssertLogOutputContains(string text)
        {
            Assert.NotEqual(-1, logWriter.Content.IndexOf(text));
        }

        private void AssertLogOutputDoesNotContain(string text)
        {
            Assert.Equal(-1, logWriter.Content.IndexOf(text));
        }

        private LogEntry GetActualLogEntry()
        {
            string entryText = logWriter.Content;

            if (entryText == null || entryText.Trim().Length == 0)
                return null;

            return JSON.Deserialize<LogEntry>(entryText);
        }
    }

    class Context
    {
        public int someArbitraryNumber { get; set; }
        public string someArbitraryText { get; set; }
    }

    class LogEntry
    {
        public string level { get; set; }
        public string message { get; set; }
        public string service { get; set; }
        public string timestamp { get; set; }
        public string exception { get; set; }
        public LogProperties properties { get; set; }
        public Context context { get; set; }
    }

    class LogProperties
    {
        public string animal { get; set; }
        public string birthday { get; set; }
        public string color { get; set; }
        public Habits habits { get; set; }
        public InterestingPropertyNames interestingPropertyNames { get; set; }
    }

    class Habits
    {
        public Diet diet { get; set; }
    }

    class Diet
    {
        public string monday { get; set; }
        public string tuesday { get; set; }
        public string wednesday { get; set; }
    }

    class InterestingPropertyNames
    {
        public string pascalCase { get; set; }
        public string with_2_Numb3rs_And_Underscores { get; set; }
    }
}
