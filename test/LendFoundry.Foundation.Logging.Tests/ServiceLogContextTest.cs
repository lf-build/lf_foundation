﻿using System;

using Xunit;
using Moq;
using System.Reflection;
#if DOTNET2
using Microsoft.Extensions.Primitives;
using Microsoft.AspNetCore.Http;
#else
using Microsoft.Framework.Primitives;
using Microsoft.AspNet.Http;
#endif
namespace LendFoundry.Foundation.Logging.Tests
{
    public class ServiceLogContextTest
    {
        private readonly ILogContext logContext;

        private const string ExpectedRequestId = "YYZ-2112";

        public ServiceLogContextTest()
        {
            IHttpContextAccessor httpContextAccessor = CreateHttpContextAccessorWithHttpHeader(ServiceLogContext.XRequestIdHeader, ExpectedRequestId);
            logContext = new ServiceLogContext(httpContextAccessor);
        }

        private static IHttpContextAccessor CreateHttpContextAccessorWithHttpHeader(string httpHeader, string value)
        {
            Mock<IHttpContextAccessor> mockHttpContextAccessor = new Mock<IHttpContextAccessor>();
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            Mock<HttpRequest> mockHttpRequest = new Mock<HttpRequest>();
            Mock<IHeaderDictionary> mockHttpHeaders = new Mock<IHeaderDictionary>();

            mockHttpContextAccessor.Setup(accessor => accessor.HttpContext).Returns(mockHttpContext.Object);
            mockHttpContext.Setup(context => context.Request).Returns(mockHttpRequest.Object);
            mockHttpRequest.Setup(request => request.Headers).Returns(mockHttpHeaders.Object);
            mockHttpHeaders.Setup(headers => headers[httpHeader]).Returns(new StringValues(value));

            return mockHttpContextAccessor.Object;
        }

        [Fact]
        public void DataIsNotNull()
        {
            Assert.NotNull(logContext.Data);
        }

        [Fact]
        public void DataContainsRequestIdProperty()
        {
            Assert.NotNull(GetProperty("requestId", logContext.Data));
        }

        [Fact]
        public void RequestIdIsEqualToTheXRequestIdHeader()
        {
            Assert.Equal(ExpectedRequestId, GetPropertyValue("requestId", logContext.Data));
        }

        private static object GetPropertyValue(string propertyName, object obj)
        {
            return GetProperty(propertyName, obj).GetValue(obj);
        }

        private static PropertyInfo GetProperty(string propertyName, object obj)
        {
            Type dataType = obj.GetType();
            var property = dataType.GetProperty(propertyName);
            return property;
        }
    }
}
