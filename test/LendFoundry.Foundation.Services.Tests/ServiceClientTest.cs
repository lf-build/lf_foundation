﻿using System;
using Jil;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using Moq;
using RestSharp;
using Xunit;
using LendFoundry.Foundation.Client;
using System.Collections.Generic;
#if DOTNET2
using Microsoft.AspNetCore.Http;
#else
using Microsoft.AspNet.Http;
#endif
namespace LendFoundry.Foundation.Services.Tests
{
    public class ServiceClientTest
    {

        [Fact]
        public void ShouldFailWithNullEndpoint()
        {
            var accessor = Mock.Of<IHttpContextAccessor>();
            var logger = Mock.Of<ILogger>();
            var tokenReader = Mock.Of<ITokenReader>();
            Assert.Throws<ArgumentNullException>(() => new ServiceClient(accessor, logger, tokenReader, (Uri)null));
        }

        [Fact]
        public void ShouldAcceptSimpleEndpoint()
        {
            var accessor = Mock.Of<IHttpContextAccessor>();
            var logger = Mock.Of<ILogger>();
            var tokenReader = Mock.Of<ITokenReader>();
            var client = new ServiceClient(accessor, logger, tokenReader, new Uri("http://test"));
            Assert.NotNull(client);
        }


        private class MyClient
        {
            public MyClient(IServiceClient client)
            {
                Client = client;
            }

            private IServiceClient Client { get; set; }

            public bool Do()
            {
                return Client.Execute(new RestRequest());
            }

            public MyReturn DoWithReturn()
            {
                var request = new RestRequest("/test", Method.POST);
                return Client.Execute<MyReturn>(request);
            }
        }

        public class MyReturn
        {
            public string Value { get; set; }

            public override bool Equals(object obj)
            {
                var other = obj as MyReturn;
                return other != null && other.Value == Value;
            }

            public override int GetHashCode()
            {
                return -1937169414 + EqualityComparer<string>.Default.GetHashCode(Value);
            }
        }
    }
}