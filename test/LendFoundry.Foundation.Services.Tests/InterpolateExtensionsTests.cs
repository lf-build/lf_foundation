﻿using LendFoundry.Foundation.Services.Handlebars;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace LendFoundry.Foundation.Services.Tests
{
    public class InterpolateExtensionsTests
    {
        [Fact]
        public void InterpolateExtensions_HasRegex()
        {
            var configuration = new 
            {
                EventName = "PaymentAdded",
                EntityId = "{Data.LoanReferenceNumber}",
                Title = "#PaymentId={Data.PaymentId} #Method={Data.Method} #Type={Data.Type} #DueDate={Data.DueDate} #Code={Data.Code} #Amount={Data.Amount} #Status={Data.Status} #Attempts={Data.Attempts}",
                Description = "Payment of ${Data.Amount} via ${Data.Method} added as ${Data.Type} for ${Data.DueDate}"
            };

            var eventInfo = new 
            {
                Data = Newtonsoft.Json.Linq.JObject.FromObject(new
                {
                    LoanReferenceNumber = "0002",
                    DueDate = Convert.ToDateTime("2016-04-15"),
                    Amount = 337.92,
                    Type = "Scheduled",
                    Status = "Pending",
                    Attempts = 1,
                    Code = 124,
                    Method = "ACH",
                    PaymentId = "56a1951d86f89128308736a9",
                    Notes = "PaymentAdded fake by Postman"
                })
                .ToObject(typeof(Newtonsoft.Json.Linq.JObject)),
                Name = "PaymentAdded"
            };

            var regex = new System.Text.RegularExpressions.Regex(@"\{([A-Za-z0-9_]+(\.[A-Za-z0-9_]+)*)\}");

            var entityId = configuration.EntityId.FormatWith(eventInfo, regex);
            Assert.NotEmpty(entityId);
        }

        [Fact]
        public void InterpolateExtensions_HasNoMatchWithRegex()
        {
            var configuration = new 
            {
                EventName = "PaymentAdded",
                EntityId = "{Data.LoanReferenceNumber}",
                Title = "#PaymentId={Data.PaymentId} #Method={Data.Method} #Type={Data.Type} #DueDate={Data.DueDate} #Code={Data.Code} #Amount={Data.Amount} #Status={Data.Status} #Attempts={Data.Attempts}",
                Description = "Payment of ${Data.Amount} via ${Data.Method} added as ${Data.Type} for ${Data.DueDate}"
            };

            var eventInfo = new 
            {
                Data = Newtonsoft.Json.Linq.JObject.FromObject(new
                {
                    LoanReferenceNumber = "0002",
                    DueDate = Convert.ToDateTime("2016-04-15"),
                    Amount = 337.92,
                    Type = "Scheduled",
                    Status = "Pending",
                    Attempts = 1,
                    Code = 124,
                    Method = "ACH",
                    PaymentId = "56a1951d86f89128308736a9",
                    Notes = "PaymentAdded fake by Postman"
                })
                .ToObject(typeof(Newtonsoft.Json.Linq.JObject)),
                Name = "PaymentAdded"
            };

            var regex = new System.Text.RegularExpressions.Regex(@"\{([A-Za-z]*)\}");

            var entityId = configuration.EntityId.FormatWith(eventInfo, regex);
            Assert.NotEmpty(entityId);
            Assert.Contains("{Data.LoanReferenceNumber}", entityId);
        }

        [Fact]
        public void InterpolateExtensions_UsingHandleBars()
        {
            var handlebarsResolver = new HandlebarsResolver();
            handlebarsResolver.Start();

            var configuration = new
            {
                EventName = "PaymentAdded",
                EntityId = "{Data.LoanReferenceNumber}",
                Title = "#PaymentId={Data.PaymentId} #Method={Data.Method} #Type={Data.Type} #DueDate={Data.DueDate} #Code={Data.Code} #Amount={Data.Amount} #Status={Data.Status} #Attempts={Data.Attempts}",
                Description = "Payment of {{#ifc Method = 'ACH'}}${Data.Amount}{{/ifc}} via ${Data.Method} added",
                Tags = new[] { "UnitTests" }
            };

            var eventInfo = new
            {
                Data = Newtonsoft.Json.Linq.JObject.FromObject(new
                {
                    LoanReferenceNumber = "0002",
                    DueDate = Convert.ToDateTime("2016-04-15"),
                    Amount = 337.92,
                    Type = "Scheduled",
                    Status = "Pending",
                    Attempts = 1,
                    Code = 124,
                    Method = "ACH",
                    PaymentId = "56a1951d86f89128308736a9",
                    Notes = "PaymentAdded fake by Postman"
                })
                .ToObject(typeof(Newtonsoft.Json.Linq.JObject)),
                Name = "PaymentAdded"
            };

            var entityId = configuration.EntityId.FormatWith(eventInfo);
            Assert.NotEmpty(entityId);

            var description = configuration.Description.FormatWith(eventInfo);
            Assert.NotEmpty(description);
            Assert.Contains("Payment of $337.92 via $ACH added", description);
        }

        [Fact]
        public void InterpolateExtensions_NotFoundPropertyName()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var configuration = new
                {
                    EventName = "PaymentAdded",
                    EntityId = "{Data.LoanReferenceNumber}",
                    Title = "#PaymentId={Data.PaymentId} #Method={Data.Method} #Type={Data.Type} #DueDate={Data.DueDate} #Code={Data.Code} #Amount={Data.Amount} #Status={Data.Status} #Attempts={Data.Attempts}",
                    Description = "{Data.Reason} Payment of {{#ifc Method = 'ACH'}}${Data.Amount}{{/ifc}} via ${Data.Method} added",
                    Tags = new[] { "UnitTests" }
                };

                var eventInfo = new
                {
                    Data = Newtonsoft.Json.Linq.JObject.FromObject(new
                    {
                        LoanReferenceNumber = "0002",
                        DueDate = Convert.ToDateTime("2016-04-15"),
                        Amount = 337.92,
                        Type = "Scheduled",
                        Status = "Pending",
                        Attempts = 1,
                        Code = 124,
                        Method = "ACH",
                        PaymentId = "56a1951d86f89128308736a9",
                        Notes = "PaymentAdded fake by Postman"
                    })
                    .ToObject(typeof(Newtonsoft.Json.Linq.JObject)),
                    Name = "PaymentAdded"
                };

                var entityId = configuration.EntityId.FormatWith(eventInfo);
                Assert.NotNull(entityId);

                configuration.Description.FormatWith(eventInfo);
            });
        }

        [Fact]
        public void InterpolateExtensions_PropertyValueHasHandleBars()
        {
            var handlebarsResolver = new HandlebarsResolver();
            handlebarsResolver.Start();

            var configuration = new
            {
                EventName = "PaymentAdded",
                EntityId = "{Data.LoanReferenceNumber}",
                Title = "#PaymentId={Data.PaymentId} #Method={Data.Method} #Type={Data.Type} #DueDate={Data.DueDate} #Code={Data.Code} #Amount={Data.Amount} #Status={Data.Status} #Attempts={Data.Attempts}",
                Description = "Payment of {Data.Notes} via ${Data.Method} added",
                Tags = new[] { "UnitTests" }
            };

            var eventInfo = new
            {
                Data = Newtonsoft.Json.Linq.JObject.FromObject(new
                {
                    LoanReferenceNumber = "0002",
                    DueDate = Convert.ToDateTime("2016-04-15"),
                    Amount = 337.92,
                    Type = "Scheduled",
                    Status = "Pending",
                    Attempts = 1,
                    Code = 124,
                    Method = "ACH",
                    PaymentId = "56a1951d86f89128308736a9",
                    Notes = "{{#ifc Method = 'ACH'}}$337.92{{/ifc}}"
                })
                .ToObject(typeof(Newtonsoft.Json.Linq.JObject)),
                Name = "PaymentAdded"
            };

            var entityId = configuration.EntityId.FormatWith(eventInfo);
            Assert.NotNull(entityId);

            var description = configuration.Description.FormatWith(eventInfo);
            Assert.NotNull(description);
            Assert.Contains("Payment of $337.92 via $ACH added", description);
        }

        [Fact]
        public void InterpolateExtensions_InvalidObject()
        {
            Assert.Throws<ArgumentException>(() => 
            {
                var handlebarsResolver = new HandlebarsResolver();
                handlebarsResolver.Start();

                var configuration = new
                {
                    EventName = "PaymentAdded",
                    EntityId = "{{#ifc Method = 'ACH'}}$337.92{{/ifc}}{Data.LoanReferenceNumber}",
                    Title = "#PaymentId={Data.PaymentId} #Method={Data.Method} #Type={Data.Type} #DueDate={Data.DueDate} #Code={Data.Code} #Amount={Data.Amount} #Status={Data.Status} #Attempts={Data.Attempts}",
                    Description = "Payment of {Data.Notes} via ${Data.Method} added",
                    Tags = new[] { "UnitTests" }
                };

                var eventInfo = new
                {
                    Data = new object[]
                    {
                        new
                        {
                            LoanReferenceNumber = "0002",
                            DueDate = Convert.ToDateTime("2016-04-15"),
                            Amount = 337.92,
                            Type = "Scheduled",
                            Status = "Pending",
                            Attempts = 1,
                            Code = 124,
                            Method = "ACH",
                            PaymentId = "56a1951d86f89128308736a9",
                            Notes = "{{#ifc Method = 'ACH'}}$337.92{{/ifc}}"
                        }
                    },
                    Name = "PaymentAdded"
                };

                var entityId = configuration.EntityId.FormatWith(eventInfo);
            });
        }
    }
}