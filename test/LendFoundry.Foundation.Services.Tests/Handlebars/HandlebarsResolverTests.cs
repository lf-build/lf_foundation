﻿using LendFoundry.Foundation.Services.Handlebars;
using Moq;
using System;
using Xunit;
#if DOTNET2
using Microsoft.AspNetCore.Builder;
#else
using Microsoft.AspNet.Builder;
#endif
namespace LendFoundry.Foundation.Services.Tests.Handlebars
{
    public class HandlebarsResolverTests
    {
        [Fact]
        public void Start_HandlebarsResolver()
        {
            var handlebarsResolver = new HandlebarsResolver();

            Assert.NotNull(handlebarsResolver);

            handlebarsResolver.Start();
        }

        [Fact]
        public void UseHandleBars_InvalidOperation()
        {
            Assert.Throws<InvalidOperationException>(() =>
            {
                var handlebarsResolver = new HandlebarsResolver();
                var applicationBuilder = new Mock<IApplicationBuilder>();
                var serviceProvider = new Mock<IServiceProvider>();

                serviceProvider.Setup(x => x.GetRequiredService<IHandlebarsResolver>())
                    .Returns(handlebarsResolver);

                applicationBuilder.Setup(x => x.ApplicationServices)
                    .Returns(serviceProvider.Object);

                HandlebarsExtendedMapExtensions.UseHandlebars(applicationBuilder.Object);
            });
        }
    }

    public interface IServiceProvider : System.IServiceProvider
    {
        T GetService<T>() where T : class;

        T GetRequiredService<T>() where T : class;
    }
}