﻿using HandlebarsDotNet;
using Newtonsoft.Json;
using System.Dynamic;
using Xunit;

namespace LendFoundry.Foundation.Services.Tests.Handlebars
{
    public class HandlebarsTest
    {
        private static void RegisterHandlebarsHelpers()
        {
            HandlebarsDotNet.Handlebars.RegisterHelper("ifc", (writer, options, context, arguments) =>
            {
                string arg1 = (string)arguments[0];
                string arg2 = (string)arguments[1];

                if (arg1.Equals(arg2))
                {
                    options.Template(writer, (object)context);
                }
                else
                {
                    options.Inverse(writer, (object)context);
                }
            });
        }

        [Fact]
        public void Json_Test()
        {
            RegisterHandlebarsHelpers();

            var json = "{\"Title\" : \"My Test\",\"LoanReferenceNumber\" : \"{{#ifc Data.Flag ='ok'}}loan001{{else}}loan002{{/ifc}}\",\"Description\" : \"message\", \"Data\" : { \"Flag\" : \"ok\"}}";

            // System.Web.Helpers.Json.Decode
            var obj = JsonConvert.DeserializeObject<ExpandoObject>(json);

            var source = "{{#ifc Data.Flag ='ok'}}loan001{{else}}loan002{{/ifc}}";
            var template = HandlebarsDotNet.Handlebars.Compile(source);

            var result = template(obj);

            Assert.Equal("loan001", result);
        }

        [Fact]
        public void Handlebars_True()
        {
            RegisterHandlebarsHelpers();

            var eventhubobject = new
            {
                Title = "Title",
                LoanReferenceNumber = "{{#ifC Description ='extra'}}loan001{{else}}loan002{{/ifC}}",
                Description = "extra"
            };

            var template = HandlebarsDotNet.Handlebars.Compile(eventhubobject.LoanReferenceNumber);

            var result = template(eventhubobject);

            Assert.Equal("loan001", result);
        }

        [Fact]
        public void Handlebars_False()
        {
            RegisterHandlebarsHelpers();

            var eventhubobject = new
            {
                Title = "{{#ifC Description ='extra'}}extra payment made{{else}}new payment{{/ifC}}",
                LoanReferenceNumber = "loan001",
                Description = ""
            };

            var template = HandlebarsDotNet.Handlebars.Compile(eventhubobject.Title);

            var result = template(eventhubobject);

            Assert.Equal("new payment", result);
        }

        [Fact]
        public void Invalid_Test()
        {
            RegisterHandlebarsHelpers();

            Assert.Throws<HandlebarsRuntimeException>(() =>
            {
                var json = "{{if Description ='extra'}}extra payment made{{else}}new payment{{/ifCond}}";

                var template = HandlebarsDotNet.Handlebars.Compile(json);

                var trueData = new
                {
                    arg1 = "extra"
                };

                template(trueData);
            });
        }
    }
}