﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Text.RegularExpressions;
using Xunit;

namespace LendFoundry.Foundation.Services.Tests.Handlebars
{
    public class HandlebarsVsRegexTest
    {
        private static readonly Regex HbRegex = new Regex("(?<replaceGroup>{{[^}]*}})", RegexOptions.Compiled);

        [Fact]
        public void Compare_Handlebars_Against_Regex()
        {
            var compare = new List<KeyValuePair<long, string>>();

            var str = "This is {{Test}} of {{This}}, and this is {{Test}} of {{This}}, and this is {{Test}} of {{This}}, and this is {{Test}} of {{This}}, and this is {{Test}} of {{This}}"
                + "This is {{Test}} of {{This}}, and this is {{Test}} of {{This}}, and this is {{Test}} of {{This}}, and this is {{Test}} of {{This}}, and this is {{Test}} of {{This}}"
                + "This is {{Test}} of {{This}}, and this is {{Test}} of {{This}}, and this is {{Test}} of {{This}}, and this is {{Test}} of {{This}}, and this is {{Test}} of {{This}}"
                + "This is {{Test}} of {{This}}, and this is {{Test}} of {{This}}, and this is {{Test}} of {{This}}, and this is {{Test}} of {{This}}, and this is {{Test}} of {{This}}";

            var classicPoco = new
            {
                Test = "Hundle",
                This = "Regex"
            };

            var template = HandlebarsDotNet.Handlebars.Compile(str);

            var sw = new Stopwatch();

            sw.Start();
            var str1 = template(classicPoco);
            compare.Add(new KeyValuePair<long, string>(sw.ElapsedMilliseconds, $"Handlebars Test: {sw.ElapsedTicks / 10} microseconds ({sw.ElapsedMilliseconds} milliseconds)"));
            sw.Restart();

            sw.Start();
            var str2 = RegexPoco(str, classicPoco);
            compare.Add(new KeyValuePair<long, string>(sw.ElapsedMilliseconds, $"Regex Test: {sw.ElapsedTicks / 10} microseconds ({sw.ElapsedMilliseconds} milliseconds)"));
            sw.Restart();
        }

        public static string RegexPoco(string str, object dataset)
        {
            foreach (Match v in HbRegex.Matches(str))
            {
                if (!v.Success)
                {
                    continue;
                }

                var key = v.Value.Trim('{', '}');
                var value = dataset.GetType().GetProperty(key).GetValue(dataset);
                str = str.Replace(v.Value, value.ToString());
            }

            return str;
        }
    }
}