﻿using LendFoundry.Foundation.Logging;
using Moq;
using System;
using System.Threading.Tasks;
using Xunit;
using LendFoundry.Foundation.Client;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif
namespace LendFoundry.Foundation.Services.Tests
{
    public class ExtendedControllerTest
    {
        [Fact]
        public void InvalidArgumentExceptionIsRaised()
        {
            var logger = new Mock<ILogger>();
            var behavior = new Mock<IBehavior>();
            behavior.Setup(x => x.Run()).Throws(new InvalidArgumentException(""));

            var act = new Controller(logger.Object, behavior.Object);
            var result = (ErrorResult)act.RequestIt();

            logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
            Assert.True(result.StatusCode == 400);         
        }

        [Fact]
        public void InvalidOperationExceptionIsRaised()
        {
            var logger = new Mock<ILogger>();
            var behavior = new Mock<IBehavior>();
            behavior.Setup(x => x.Run()).Throws(new InvalidOperationException(""));

            var act = new Controller(logger.Object, behavior.Object);
            var result = (ErrorResult)act.RequestIt();

            logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
            Assert.True(result.StatusCode == 400);
        }

        [Fact]
        public void ArgumentNullExceptionIsRaised()
        {
            var logger = new Mock<ILogger>();
            var behavior = new Mock<IBehavior>();
            behavior.Setup(x => x.Run()).Throws(new ArgumentNullException(""));

            var act = new Controller(logger.Object, behavior.Object);
            var result = (ErrorResult)act.RequestIt();

            logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
            Assert.True(result.StatusCode == 400);
        }

        [Fact]
        public void ArgumentExceptionIsRaised()
        {
            var logger = new Mock<ILogger>();
            var behavior = new Mock<IBehavior>();
            behavior.Setup(x => x.Run()).Throws(new ArgumentException(""));

            var act = new Controller(logger.Object, behavior.Object);
            var result = (ErrorResult)act.RequestIt();

            logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
            Assert.True(result.StatusCode == 400);
        }

        [Fact]
        public void NotFoundExceptionIsRaised()
        {
            var logger = new Mock<ILogger>();
            var behavior = new Mock<IBehavior>();
            behavior.Setup(x => x.Run()).Throws(new NotFoundException(""));

            var act = new Controller(logger.Object, behavior.Object);
            var result = (ErrorResult)act.RequestIt();

            logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
            Assert.True(result.StatusCode == 404);
        }

        [Fact]
        public void ClientExceptionIsRaised()
        {
            var logger = new Mock<ILogger>();
            var behavior = new Mock<IBehavior>();
            behavior.Setup(x => x.Run()).Throws(new ClientException("", new Error(500, "")));

            var act = new Controller(logger.Object, behavior.Object);
            var result = (ErrorResult)act.RequestIt();

            logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
            Assert.True(result.StatusCode == 500);
        }

        [Fact]
        public void ExceptionIsRaisedAndHasNoLoggerReference()
        {            
            var behavior = new Mock<IBehavior>();
            behavior.Setup(x => x.Run()).Throws(new ClientException("", new Error(500, "")));

            var act = new Controller(behavior.Object);
            var result = (ErrorResult)act.RequestIt();
            
            Assert.True(result.StatusCode == 500);
        }

        [Fact]
        public void WhenRisingPersonalException()
        {
            var behavior = new Mock<IBehavior>();
            behavior.Setup(x => x.Run()).Throws(new MyException());

            var act = new Controller(behavior.Object);
            var result = (ErrorResult)act.RequestIt();

            Assert.True(result.StatusCode == 400);
        }


        [Fact]
        public void AsyncInvalidArgumentExceptionIsRaised()
        {
            var logger = new Mock<ILogger>();
            var behavior = new Mock<IBehaviorAsync>();
            behavior.Setup(x => x.RunAsync()).Throws(new InvalidArgumentException(""));

            var act = new ControllerAsync(logger.Object, behavior.Object);
            var result = (ErrorResult)act.RequestItAsync().Result;

            logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
            Assert.True(result.StatusCode == 400);
        }

        [Fact]
        public void AsyncInvalidOperationExceptionIsRaised()
        {
            var logger = new Mock<ILogger>();
            var behavior = new Mock<IBehaviorAsync>();
            behavior.Setup(x => x.RunAsync()).Throws(new InvalidOperationException(""));

            var act = new ControllerAsync(logger.Object, behavior.Object);
            var result = (ErrorResult)act.RequestItAsync().Result;

            logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
            Assert.True(result.StatusCode == 400);
        }

        [Fact]
        public void AsyncArgumentNullExceptionIsRaised()
        {
            var logger = new Mock<ILogger>();
            var behavior = new Mock<IBehaviorAsync>();
            behavior.Setup(x => x.RunAsync()).Throws(new ArgumentNullException(""));

            var act = new ControllerAsync(logger.Object, behavior.Object);
            var result = (ErrorResult)act.RequestItAsync().Result;

            logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
            Assert.True(result.StatusCode == 400);
        }

        [Fact]
        public void AsyncArgumentExceptionIsRaised()
        {
            var logger = new Mock<ILogger>();
            var behavior = new Mock<IBehaviorAsync>();
            behavior.Setup(x => x.RunAsync()).Throws(new ArgumentException(""));

            var act = new ControllerAsync(logger.Object, behavior.Object);
            var result = (ErrorResult)act.RequestItAsync().Result;

            logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
            Assert.True(result.StatusCode == 400);
        }

        [Fact]
        public void AsyncNotFoundExceptionIsRaised()
        {
            var logger = new Mock<ILogger>();
            var behavior = new Mock<IBehaviorAsync>();
            behavior.Setup(x => x.RunAsync()).Throws(new NotFoundException(""));

            var act = new ControllerAsync(logger.Object, behavior.Object);
            var result = (ErrorResult)act.RequestItAsync().Result;

            logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
            Assert.True(result.StatusCode == 404);
        }

        [Fact]
        public void AsyncClientExceptionIsRaised()
        {
            var logger = new Mock<ILogger>();
            var behavior = new Mock<IBehaviorAsync>();
            behavior.Setup(x => x.RunAsync()).Throws(new ClientException("", new Error(500, "")));

            var act = new ControllerAsync(logger.Object, behavior.Object);
            var result = (ErrorResult)act.RequestItAsync().Result;

            logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
            Assert.True(result.StatusCode == 500);
        }

        [Fact]
        public void AsyncExceptionIsRaisedAndHasNoLoggerReference()
        {
            var behavior = new Mock<IBehaviorAsync>();
            behavior.Setup(x => x.RunAsync()).Throws(new ClientException("", new Error(500, "")));

            var act = new ControllerAsync(behavior.Object);
            var result = (ErrorResult)act.RequestItAsync().Result;

            Assert.True(result.StatusCode == 500);
        }

        [Fact]
        public void AsyncWhenRisingPersonalException()
        {
            var behavior = new Mock<IBehaviorAsync>();
            behavior.Setup(x => x.RunAsync()).Throws(new MyException());

            var act = new ControllerAsync(behavior.Object);
            var result = (ErrorResult)act.RequestItAsync().Result;

            Assert.True(result.StatusCode == 400);
        }


        public class Controller : ExtendedController
        {
            public Controller(IBehavior behavior)
            {
                Behavior = behavior;
            }

            public Controller(ILogger logger, IBehavior behavior) : base(logger)
            {
                Behavior = behavior;
            }

            private IBehavior Behavior { get; }

            public object RequestIt()
            {
                return Execute(() =>
                {
                    try
                    {
                        return Behavior.Run();
                    }
                    catch (MyException)
                    {
                        throw new ArgumentException();
                    }
                });
            }
        }

        public class ControllerAsync : ExtendedController
        {
            public ControllerAsync(IBehaviorAsync behavior)
            {
                Behavior = behavior;
            }

            public ControllerAsync(ILogger logger, IBehaviorAsync behavior) : base(logger)
            {
                Behavior = behavior;
            }            

            private IBehaviorAsync Behavior { get; }

            public async Task<object> RequestItAsync()
            {
                return await ExecuteAsync(() =>
                {
                    try
                    {
                        return Behavior.RunAsync();
                    }
                    catch (MyException)
                    {
                        throw new ArgumentException();
                    }
                });
            }
        }

        public interface IBehavior
        {
            IActionResult Run();
        }

        public interface IBehaviorAsync
        {
            Task<IActionResult> RunAsync();
        }

        public class MyException : Exception
        {
        }
    }
}
