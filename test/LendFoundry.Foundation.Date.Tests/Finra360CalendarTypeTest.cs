﻿using LendFoundry.Foundation.Date;
using System;
using Xunit;

namespace LendFoundry.Amortization.Engine.CalendarTypes.Tests
{
    public class Finra360CalendarTypeTest
    {
        private IFinancialCalendar financialCalendar = new FinraCalendar();

        [Fact]
        public void From1stTo30thOfSameMonthItIsWorth29DaysOfInterest()
        {
            AssertDaysBetween(29, January(1), January(30));
            AssertDaysBetween(29,   March(1),   March(30));
            AssertDaysBetween(29,   April(1),   April(30));
        }

        [Fact]
        public void From1stTo31stOfSameMonthItIsWorth30DaysOfInterest()
        {
            AssertDaysBetween(30, January(1), January(31));
            AssertDaysBetween(30,   March(1),   March(31));
            AssertDaysBetween(30,     May(1),     May(31));
        }

        [Fact]
        public void From1stTo1stOfNextMonthItIsWorth30DaysOfInterest()
        {
            AssertDaysBetween(30,  January(1), February(1));
            AssertDaysBetween(30, February(1),    March(1));
            AssertDaysBetween(30,    March(1),    April(1));
            AssertDaysBetween(30,    April(1),      May(1));
        }

        [Fact]
        public void February1stTo28thIsWorth27DaysOfInterest()
        {
            AssertDaysBetween(27, February(1), February(28));
        }

        [Fact]
        public void February23thToMarch3thIsWorth27DaysOfInterest()
        {
            AssertDaysBetween(10, February(23), March(3));
        }

        [Fact]
        public void May15thToJune6thIsWorth21DaysOfInterest()
        {
            AssertDaysBetween(21, May(15), June(6));
        }

        [Fact]
        public void From30thOr31stTo1stIsWorth1DayOfInterest()
        {
            AssertDaysBetween(1, May(30), June(1));
            AssertDaysBetween(1, May(31), June(1));
        }

        [Fact]
        public void From30thOr31stTo30thOfNextMonthIsWorth30DaysOfInterest()
        {
            AssertDaysBetween(30, July(30), August(30));
            AssertDaysBetween(30, July(31), August(30));
        }

        [Fact]
        public void From30thOr31stTo31stOfNextMonthIsWorth30DaysOfInterest()
        {
            AssertDaysBetween(30, July(30), August(31));
            AssertDaysBetween(30, July(31), August(31));
        }

        [Fact]
        public void From30thOr31stTo1stOfSecondFollowingMonthIsWorth31DaysOfInterest()
        {
            AssertDaysBetween(31, July(30), September(1));
            AssertDaysBetween(31, July(31), September(1));
        }

        private void AssertDaysBetween(int expectedDaysBetween, DateTimeOffset start, DateTimeOffset end)
        {
            Assert.Equal(expectedDaysBetween, financialCalendar.DaysBetween(start, end));
        }

        private static DateTimeOffset January(int day) { return Date(2015, 1, day); }
        private static DateTimeOffset February(int day) { return Date(2015, 2, day); }
        private static DateTimeOffset March(int day) { return Date(2015, 3, day); }
        private static DateTimeOffset April(int day) { return Date(2015, 4, day); }
        private static DateTimeOffset May(int day) { return Date(2015, 5, day); }
        private static DateTimeOffset June(int day) { return Date(2015, 6, day); }
        private static DateTimeOffset July(int day) { return Date(2015, 7, day); }
        private static DateTimeOffset August(int day) { return Date(2015, 8, day); }
        private static DateTimeOffset September(int day) { return Date(2015, 9, day); }

        private static DateTimeOffset Date(int year, int month, int day)
        {
            return new DateTimeOffset(year, month, day, 0, 0, 0, TimeSpan.Zero);
        }
    }
}
